/*
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2018 polymorph.cool
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

// *********************************
// ************ CONFIG *************
// *********************************

var enable_sysiphus_avatar = false;

if ( enable_sysiphus_avatar ) {
	var config_sysiphus_avatar = new SysiphusAvatarConfig();
	config_sysiphus_avatar.mesh = "Body_Idle";
	config_sysiphus_avatar.skeleton_root = "Armature_breathing";
	config_sysiphus_avatar.path_model = 'models/geraldine.dae';
	config_sysiphus_avatar.path_animations = ['models/geraldine_jump_back.dae'];
	config_sysiphus_avatar.name_animations = ["jump back"];
	config_sysiphus_avatar.name_default_animation = "jump back";
	config_sysiphus_avatar.path_deforms = ['presets/deform_test.json','presets/deform_test2.json'];
}

var config_pantonpouf = new PantonPoufConfig();
config_pantonpouf.morph_path = 'models/pouf.json';
config_pantonpouf.morph_default = 'basis';
config_pantonpouf.ready_callbak = on_pouf_ready;
config_pantonpouf.butt_anim_path = 'models/butt_anims.json';

// *********************************
// ************ GLOBALS ************
// *********************************

var stats_enabled = true;

var container, stats, clock, controls;
var camera, scene, renderer;

var gridHelper = undefined;

var SCENE_LOADED = false;

var avatar = undefined;
var pouf = undefined;

// debug objects
var frame_count = 0;

var ui_settings = undefined;

var texture_canvas = undefined;
var texture_context = undefined;
var texture_texture = undefined;
var texture_cellnum = 32;

// *********************************
// *********** FUNCTIONS ***********
// *********************************

init();

animate();

function init() {

	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.position.set( 5, 2.5, 5 );
	scene = new THREE.Scene();
	clock = new THREE.Clock();

	gridHelper = new THREE.GridHelper( 10, 20 );
	scene.add( gridHelper );

	// lightning
	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.15 );
	scene.add( ambientLight );
	var directionalLight = new THREE.DirectionalLight( 0xffffff, 2 );
	directionalLight.position.set( 3, 10, 3 );
	scene.add( directionalLight );

	directionalLight = new THREE.DirectionalLight( 0xff0000, 0.5 );
	directionalLight.position.set( -2, 3, -2 );
	scene.add( directionalLight );

	// renderer
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor( new THREE.Color( 0x232323 ), 1.3 );
	container.appendChild( renderer.domElement );

	// camera controls
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 2, 0 );
	controls.update();

	// stats
	stats = undefined;
	if ( stats_enabled ) {
		stats = new Stats();
		container.appendChild( stats.dom );
	}

	// debug
	var geometry;
	var material;
	var wireframe;

	// avatar
	if ( enable_sysiphus_avatar ) {
		avatar = new SysiphusAvatar();
		avatar.ready_callbak = on_avatar_ready;
		avatar.configure( config_sysiphus_avatar );
		avatar.load( scene );
	}

	pouf = new PantonPouf();
	pouf.configure( config_pantonpouf );
	pouf.load( scene );

	gridHelper.position.set( 0, 1, 0 );

	// events
	window.addEventListener( 'resize', onWindowResize, false );

}

function on_avatar_ready() {

	if ( enable_sysiphus_avatar ) {
		avatar.move( 0, 1, 0 );
		avatar.obj3d_mesh.visible = false;
	}

}

function on_pouf_ready() {

	console.log( "POUF IS READY!");

	var params = {};
	for ( var k in pouf.pouf.morphTargetDictionary ) {
		params[k] = 0;
	}
	// console.log( pouf.obj3d.morphTargetDictionary );

	var gui = new dat.GUI();
	var folder = gui.addFolder( 'Morph Targets' );
	for ( var k in pouf.pouf.morphTargetDictionary ) {
		var p = folder.add( params, k, 0, 1 ).step( 0.01 );
		p.morph_name = k;
		p.onChange( function( value ) { pouf.set_influence( this.morph_name, value ); } );
	}
	folder.open();

	var grid = THREE.ImageUtils.loadTexture('textures/grid.png');
	pouf.pouf.material.map = grid;
	// pouf.pouf.material.wireframe = true;
	pouf.pouf.material.needsUpdate = true;

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

	requestAnimationFrame( animate );
	render();
	if ( stats_enabled ) {
		stats.update();
	}

}

function render() {

	var delta = clock.getDelta();

	// Interpolation.update( delta );

	if ( enable_sysiphus_avatar && avatar.ready === true ) {

		avatar.update( delta );
		
	}

	pouf.update( delta );

	renderer.render( scene, camera );

	frame_count++;

}

function print( e ) {}