/*
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2018 polymorph.cool
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

function CushinWorld() {};

CushinWorld.init = function() {

    if ( this.world === undefined ) {
    	
    	this.gravity = -9.8;
		this.default_margin = 0.05;
		this.ground = undefined;

		this.rigids = [];
		this.softs = [];
    	
		// Physics configuration
		this.collisionConfiguration = new Ammo.btSoftBodyRigidBodyCollisionConfiguration();
		this.dispatcher = new Ammo.btCollisionDispatcher( this.collisionConfiguration );
		this.broadphase = new Ammo.btDbvtBroadphase();
		this.solver = new Ammo.btSequentialImpulseConstraintSolver();
		this.softBodySolver = new Ammo.btDefaultSoftBodySolver();
		this.world = new Ammo.btSoftRigidDynamicsWorld( 
				this.dispatcher, this.broadphase, this.solver, 
				this.collisionConfiguration, this.softBodySolver);
		this.world.setGravity( new Ammo.btVector3( 0, this.gravity, 0 ) );
		this.world.getWorldInfo().set_m_gravity( new Ammo.btVector3( 0, this.gravity, 0 ) );

		this.softBodyHelpers = new Ammo.btSoftBodyHelpers();
		this.bttransform = new Ammo.btTransform();

	} else {

		console.log( "CushinWorld is already started!" );

	}

};

CushinWorld.create_ground = function( x, y, z ) {

	this.ground = this.createParalellepiped( 
		40, 1, 40, 0, 
		new THREE.Vector3( x, y, z ), 
		new THREE.Quaternion(), 
		new THREE.MeshPhongMaterial( { color: 0xFFFFFF } ) );
	this.ground.castShadow = true;
	this.ground.receiveShadow = true;

}

CushinWorld.createIndexedBufferGeometryFromGeometry = function( geometry ) {

	var numVertices = geometry.vertices.length;
	var numFaces = geometry.faces.length;

	var bufferGeom = new THREE.BufferGeometry();
	var vertices = new Float32Array( numVertices * 3 );
	var indices = new ( numFaces * 3 > 65535 ? Uint32Array : Uint16Array )( numFaces * 3 );

	for ( var i = 0; i < numVertices; i++ ) {

		var p = geometry.vertices[ i ];

		var i3 = i * 3;

		vertices[ i3 ] = p.x;
		vertices[ i3 + 1 ] = p.y;
		vertices[ i3 + 2 ] = p.z;

	}

	for ( var i = 0; i < numFaces; i++ ) {

		var f = geometry.faces[ i ];

		var i3 = i * 3;

		indices[ i3 ] = f.a;
		indices[ i3 + 1 ] = f.b;
		indices[ i3 + 2 ] = f.c;

	}

	bufferGeom.setIndex( new THREE.BufferAttribute( indices, 1 ) );
	bufferGeom.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );

	return bufferGeom;

};

CushinWorld.isEqual = function( x1, y1, z1, x2, y2, z2 ) {

	var delta = 0.000001;
	return Math.abs( x2 - x1 ) < delta &&
			Math.abs( y2 - y1 ) < delta &&
			Math.abs( z2 - z1 ) < delta;

}

CushinWorld.mapIndices = function( bufGeometry, indexedBufferGeom ) {

	// Creates ammoVertices, ammoIndices and ammoIndexAssociation in bufGeometry

	var vertices = bufGeometry.attributes.position.array;
	var idxVertices = indexedBufferGeom.attributes.position.array;
	var indices = indexedBufferGeom.index.array;

	var numIdxVertices = idxVertices.length / 3;
	var numVertices = vertices.length / 3;

	bufGeometry.ammoVertices = idxVertices;
	bufGeometry.ammoIndices = indices;
	bufGeometry.ammoIndexAssociation = [];

	for ( var i = 0; i < numIdxVertices; i++ ) {

		var association = [];
		bufGeometry.ammoIndexAssociation.push( association );

		var i3 = i * 3;

		for ( var j = 0; j < numVertices; j++ ) {

			var j3 = j * 3;
			if ( this.isEqual( idxVertices[ i3 ], idxVertices[ i3 + 1 ],  idxVertices[ i3 + 2 ],
							vertices[ j3 ], vertices[ j3 + 1 ], vertices[ j3 + 2 ] ) ) {
					association.push( j3 );
			}

		}

	}

};

CushinWorld.createParalellepiped = function( sx, sy, sz, mass, pos, quat, material, margin = undefined, static = false ) {

	var volume = new THREE.Mesh( new THREE.BoxGeometry( sx, sy, sz, 1, 1, 1 ), material );
	var shape = new Ammo.btBoxShape( new Ammo.btVector3( sx * 0.5, sy * 0.5, sz * 0.5 ) );
	if ( margin === undefined ) {
		margin = this.default_margin;
	}
	shape.setMargin( margin );

	this.createRigidBody( volume, shape, mass, pos, quat, static );

	return volume;

}

CushinWorld.createRigidBody = function( volume, physicsShape, mass, pos, quat, static = false ) {

	volume.position.copy( pos );
	volume.quaternion.copy( quat );

	var transform = new Ammo.btTransform();
	transform.setIdentity();
	transform.setOrigin( new Ammo.btVector3( pos.x, pos.y, pos.z ) );
	transform.setRotation( new Ammo.btQuaternion( quat.x, quat.y, quat.z, quat.w ) );
	var motionState = new Ammo.btDefaultMotionState( transform );

	var localInertia = new Ammo.btVector3( 0, 0, 0 );
	physicsShape.calculateLocalInertia( mass, localInertia );

	var rbInfo = new Ammo.btRigidBodyConstructionInfo( mass, motionState, physicsShape, localInertia );
	var body = new Ammo.btRigidBody( rbInfo );

	volume.userData.physicsBody = body;

	scene.add( volume );

	// if ( mass > 0 ) {

		this.rigids.push( volume );

		// Disable deactivation
		body.setActivationState( 4 );

	// }

	this.world.addRigidBody( body );

	return body;

}

CushinWorld.processGeometry = function( bufGeometry ) {

	// Obtain a Geometry
	var geometry = new THREE.Geometry().fromBufferGeometry( bufGeometry );

	// Merge the vertices so the triangle soup is converted to indexed triangles
	var vertsDiff = geometry.mergeVertices();

	// Convert again to BufferGeometry, indexed
	var indexedBufferGeom = this.createIndexedBufferGeometryFromGeometry( geometry );

	// Create index arrays mapping the indexed vertices to bufGeometry vertices
	this.mapIndices( bufGeometry, indexedBufferGeom );

}

CushinWorld.createSoftVolume = function( bufferGeom, mass, pressure, margin = undefined ) {

	this.processGeometry( bufferGeom );

	var volume = new THREE.Mesh( bufferGeom, new THREE.MeshPhongMaterial( { color: 0xFFFFFF } ) );
	volume.castShadow = true;
	volume.receiveShadow = true;
	volume.frustumCulled = false;
	scene.add( volume );

	// Volume physic object

	var volumeSoftBody = this.softBodyHelpers.CreateFromTriMesh(
			this.world.getWorldInfo(),
			bufferGeom.ammoVertices,
			bufferGeom.ammoIndices,
			bufferGeom.ammoIndices.length / 3,
			true );

	var sbConfig = volumeSoftBody.get_m_cfg();
	sbConfig.set_viterations( 40 );
	sbConfig.set_piterations( 40 );

	// Soft-soft and soft-rigid collisions
	sbConfig.set_collisions( 0x11 );

	// Friction
	sbConfig.set_kDF( 0.1 );
	// Damping
	sbConfig.set_kDP( 0.01 );
	// Pressure
	sbConfig.set_kPR( pressure );
	// Stiffness
	volumeSoftBody.get_m_materials().at( 0 ).set_m_kLST( 0.9 );
	volumeSoftBody.get_m_materials().at( 0 ).set_m_kAST( 0.9 );

	volumeSoftBody.setTotalMass( mass, false );
	if ( margin === undefined ) {
		margin = this.default_margin;
	}
	Ammo.castObject( volumeSoftBody, Ammo.btCollisionObject ).getCollisionShape().setMargin( margin );
	this.world.addSoftBody( volumeSoftBody, 1, -1 );
	volume.userData.physicsBody = volumeSoftBody;
	// Disable deactivation
	volumeSoftBody.setActivationState( 4 );

	this.softs.push( volume );

	return [ volume, volumeSoftBody ];

}

CushinWorld.update = function( delta ) {

	this.world.stepSimulation( delta, 10 );

	// Update soft volumes
	for ( var i = 0, il = this.softs.length; i < il; i++ ) {
		var volume = this.softs[ i ];
		var geometry = volume.geometry;
		var softBody = volume.userData.physicsBody;
		var volumePositions = geometry.attributes.position.array;
		var volumeNormals = geometry.attributes.normal.array;
		var association = geometry.ammoIndexAssociation;
		var numVerts = association.length;
		var nodes = softBody.get_m_nodes();
		for ( var j = 0; j < numVerts; j ++ ) {

			var node = nodes.at( j );
			var nodePos = node.get_m_x();
			var x = nodePos.x();
			var y = nodePos.y();
			var z = nodePos.z();
			var nodeNormal = node.get_m_n();
			var nx = nodeNormal.x();
			var ny = nodeNormal.y();
			var nz = nodeNormal.z();

			var assocVertex = association[ j ];

			for ( var k = 0, kl = assocVertex.length; k < kl; k++ ) {
				var indexVertex = assocVertex[ k ];
				volumePositions[ indexVertex ] = x;
				volumeNormals[ indexVertex ] = nx;
				indexVertex++;
				volumePositions[ indexVertex ] = y;
				volumeNormals[ indexVertex ] = ny;
				indexVertex++;
				volumePositions[ indexVertex ] = z;
				volumeNormals[ indexVertex ] = nz;
			}
		}

		geometry.attributes.position.needsUpdate = true;
		geometry.attributes.normal.needsUpdate = true;

	}

	// Update rigid bodies
	for ( var i = 0, il = this.rigids.length; i < il; i++ ) {
		var objThree = this.rigids[ i ];
		var objPhys = objThree.userData.physicsBody;
		var ms = objPhys.getMotionState();
		if ( ms ) {
			ms.getWorldTransform( this.bttransform );
			var p = this.bttransform.getOrigin();
			var q = this.bttransform.getRotation();
			objThree.position.set( p.x(), p.y(), p.z() );
			objThree.quaternion.set( q.x(), q.y(), q.z(), q.w() );
		} else {
			objPhys.getWorldTransform( this.bttransform );
			var p = this.bttransform.getOrigin();
			var q = this.bttransform.getRotation();
			objThree.position.set( p.x(), p.y(), p.z() );
			objThree.quaternion.set( q.x(), q.y(), q.z(), q.w() );
		}
	}

}

CushinWorld.prototype.func = function() {
    this.constructor.staticMethod();
}