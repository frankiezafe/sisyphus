/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

var Interpolation = {
	_interpolations: [],
	HALF_PI: Math.PI * 0.5
};

Interpolation.sin = function( value ) {
	return ( 1 + Math.sin( -Interpolation.HALF_PI + value * Math.PI )) * 0.5;
};

Interpolation.pow2 = function( value ) {
	return value * value;
};

Interpolation.halo = function( value ) {
	var a = Math.pow( value, 1.5 ) - 0.3;
	var sig = 1 / ( 1 + Math.exp( ( -8 + a * 16 ) ) );
	var sinus1 = ( 1 + Math.sin( -Interpolation.HALF_PI + ( sig * Math.PI ) ) ) * 0.5;
	var sinus2 = 1 - ( ( 1 + Math.sin( -Interpolation.HALF_PI + ( value * Math.PI ) ) ) * 0.5 );
	return ( sinus1 * (1-value) + sinus2 * value );
};

Interpolation.new_interpolation = function( name, init, speed, method ) {

	var inter = {
		name: name,
		current: init,
		init: init,
		target: init,
		value: init,
		speed: speed,
		method: method,
		idle: true,
		set: function( value ) {
			this.current = value;
			this.target = value;
			this.value = value;
		}
	};

	Interpolation._interpolations.push( inter );

	return inter;

};

Interpolation.update = function( delta ) {

	if ( Interpolation._interpolations === undefined ) { return; }

	for ( var i in Interpolation._interpolations ) {

		var inter = Interpolation._interpolations[i];

		if (inter.current != inter.target) {

			if ( inter.current < inter.target ) {
				inter.current += delta * inter.speed;
				if ( inter.current > inter.target ) {
					inter.current = inter.target;
				}
			} else {
				inter.current -= delta * inter.speed;
				if ( inter.current < inter.target ) {
					inter.current = inter.target;
				}
			}

			inter.value = inter.current;
			if ( inter.method !== undefined ) {
				inter.value = inter.method( inter.value );
			}

			inter.idle = false;

		} else {

			inter.idle = true;

		}

	}

};
