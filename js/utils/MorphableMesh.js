/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

/*
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 The json loaded by this class must be generated with this python script, loaded in blender:
 github.com/frankiezafe/Blender-scripts/blob/master/custom_threejs_keyshapes_export.py

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

function MorphableMesh() {

	this.path = undefined;
	this.obj3d = undefined;
	this.scene = undefined;
	this.listener = undefined;
	this.default_visibility = true;

	this.load = function() {

		var xobj = new XMLHttpRequest();
		xobj.caller = this;
		xobj.overrideMimeType("application/json");
		xobj.open('GET', this.path, true);
		xobj.onreadystatechange = function () {
			if (xobj.readyState == 4 && xobj.status == "200") {
      	// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
      	this.caller.json_load_success(xobj.responseText);
    	} else if ( xobj.status != "200" ) {
    		this.caller.json_load_failed(xobj);
    	}
		}
		xobj.send(null);

	};

	this.json_load_success = function( e ) {

		try{

			var data = JSON.parse( e );

			if ( data.vertices === undefined ) {
				console.error( "PantonPouf, no vertices in '" + this.path + "'" );
				return;
			} else if ( data.vertices.length == 0 ) {
				console.error( "PantonPouf, no vertices in '" + this.path + "'" );
				return;
			}

			function disposeArray() { this.array = null; }
			var buffgeom = new THREE.BufferGeometry();
			buffgeom.setIndex( data.indices );
			buffgeom.addAttribute( 'position', new THREE.Float32BufferAttribute( data.vertices, 3 ).onUpload( disposeArray ) );
			if ( data.normals !== undefined && data.normals.length != 0 ) {
				buffgeom.addAttribute( 'normal', new THREE.Float32BufferAttribute( data.normals, 3 ).onUpload( disposeArray ) );
			}
			if ( data.colors !== undefined && data.colors.length != 0 ) {
				buffgeom.addAttribute( 'color', new THREE.Float32BufferAttribute( data.colors, 3 ).onUpload( disposeArray ) );
			}
			if ( data.uvs !== undefined && data.uvs.length != 0 ) {
				buffgeom.addAttribute( 'uv', new THREE.Float32BufferAttribute( data.uvs, 2 ).onUpload( disposeArray ) );
			}
			buffgeom.computeBoundingSphere();

			var geom = new THREE.Geometry();
			geom.fromBufferGeometry( buffgeom );

			var has_morphs = false;
			var has_vnormals = false;

			for( var k in data.morphs ) {

				var vs = data.morphs[k].vertices;
				var fn = data.morphs[k].face_normals;
				var vn = data.morphs[k].vertex_normals;
				var l = vs.length;
				var i = 0;

				has_morphs = true;
				has_vnormals = ( vn !== undefined && vn.length == data.vertices.length );

				var frame = {
					name: k,
					vertices: []
				};

				if ( has_vnormals ) {
					frame.normals = [];
				}

				while ( i < l ) {
					frame.vertices.push( new THREE.Vector3( vs[i+0], vs[i+1], vs[i+2] ) );
					if ( has_vnormals ) {
						frame.normals.push( new THREE.Vector3( vn[i+0], vn[i+1], vn[i+2] ) );
					}
					i += 3;
				}

				geom.morphTargets.push( frame );

			}

			if ( has_vnormals ) {

				geom.vertices = geom.morphTargets[ 0 ].vertices;
				var morphTarget = geom.morphTargets[ 0 ];
				for ( var j = 0, jl = geom.faces.length; j < jl; j ++ ) {
					var face = geom.faces[ j ];
					face.vertexNormals = [
						morphTarget.normals[ face.a ],
						morphTarget.normals[ face.b ],
						morphTarget.normals[ face.c ]
					];
				}

				for ( var i = 0, l = geom.morphTargets.length; i < l; i ++ ) {
					var morphTarget = geom.morphTargets[ i ];
					var vertexNormals = [];
					for ( var j = 0, jl = geom.faces.length; j < jl; j ++ ) {
						var face = geom.faces[ j ];
						vertexNormals.push( {
							a: morphTarget.normals[ face.a ],
							b: morphTarget.normals[ face.b ],
							c: morphTarget.normals[ face.c ]
						} );
					}
					geom.morphNormals.push( { vertexNormals: vertexNormals } );
				}

			}

			// pushing
			var material = new THREE.MeshPhongMaterial( {
					color: 0xffffff,
					shininess: 100,
					morphTargets: has_morphs,
					morphNormals: has_vnormals } );

			this.obj3d = new THREE.Mesh( geom, material );
			this.obj3d.visible = this.default_visibility;
			this.scene.add( this.obj3d );

			if ( this.listener !== undefined ) {
				this.listener.morphablemesh_loaded();
			}

		} catch( exception ) {

		}

	};

	this.json_load_failed = function( e ) {

		console.error( "MorphableMesh failed to load " + this.path );

		if ( this.listener !== undefined ) {
			this.listener.morphablemesh_loaded();
		}

	};

};
