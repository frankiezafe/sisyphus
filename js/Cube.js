function Cube() {

	this.path = undefined;
	this.legs = undefined;
	this.obj3d = undefined;
	this.scene = undefined;
	this.listener = undefined;
	this.default_visibility = true;
	
	this.ready = false;
	this.ready_callbak = undefined;
	
	this.morph_keys = undefined;
	this.trackers = undefined;
	
	// data correction
	this.invert_tracker = undefined;
	
	// matrix correction
	this.correction_scale_scalar = 0.235;
	this.adjustment_rotation = new THREE.Quaternion().setFromEuler( new THREE.Euler( 0, 0, 0, 'XYZ' ) );
	this.correction_translation = new THREE.Vector3( 0,0,0 );
	this.correction_rotation = new THREE.Quaternion().setFromEuler( new THREE.Euler( Math.PI * 0.5, 0, 0, 'XYZ' ) );
	this.correction_scale = new THREE.Vector3( 
		this.correction_scale_scalar,
		-this.correction_scale_scalar,
		-this.correction_scale_scalar );
	
	this.tracker_rotation = {
		"tracker_center" : { 
			'pos': new THREE.Vector3( 0,0,0.45 ),
			'rot': new THREE.Quaternion().setFromEuler( new THREE.Euler( Math.PI * 0.5,0, 0, 'XYZ' ) ),
			'scl': new THREE.Vector3( 1, 1, 1 )
		},
		"tracker_foot_left" : { 
			'pos': new THREE.Vector3( 0,0,0.05 ),
			'rot': new THREE.Quaternion().setFromEuler( new THREE.Euler( 0.6, Math.PI * -0.5, Math.PI, 'XYZ' ) ),
			'scl': new THREE.Vector3( 1, 1, 1 )
		},
		"tracker_foot_right" : { 
			'pos': new THREE.Vector3( 0,0,0.05 ),
			'rot': new THREE.Quaternion().setFromEuler( new THREE.Euler( 0.6, Math.PI * -0.5, Math.PI, 'XYZ' ) ),
			'scl': new THREE.Vector3( 1, 1, 1 )
		}
	};
	
	this.bones_rotations = undefined;
	this.bones_positions = undefined;
	
	// time management
	this.playing = true;
	this.current_frame = 0;
	this.speed_frame = 5;
	this.total_frames = 0;
	this.maximum_frame = 0;
	this.percent = 0;
	
	this.wanderer = undefined;
	
	// JSON data
	this.morph_data = undefined;
	this.legs_data = undefined;
	
	this.unload = function() {
		
		if ( this.obj3d != undefined ) {
			this.scene.remove( this.obj3d );
		}
		
		if ( this.trackers != undefined ) {
			for ( k in this.trackers ) {
				this.scene.remove( this.trackers[k]['obj'] );
			}
		}
		
		this.ready = false;
		this.obj3d = undefined;
		this.morph_keys = undefined;
		this.trackers = undefined;
		
		this.bones_rotations = undefined;
		this.bones_positions = undefined;
		this.morph_data = undefined;
		this.legs_data = undefined;
		
		this.current_frame = 0;
		this.percent = 0;
		
	}

	this.load = function() {
		
		this.unload();
		
		var xobj0 = new XMLHttpRequest();
		xobj0.caller = this;
		xobj0.overrideMimeType("application/json");
		xobj0.open('GET', this.path, true);
		xobj0.onreadystatechange = function () {
			if (xobj0.readyState == 4 && xobj0.status == "200") {
				// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
				this.caller.on_morph_loaded(xobj0.responseText);
			} else if ( xobj0.status != "200" ) {
				//this.caller.json_load_failed(xobj0);
			}
		}
		xobj0.send(null);
		
		if ( this.legs != undefined ) {
			
			//console.log( this.legs );
			
			var xobj1 = new XMLHttpRequest();
			xobj1.caller = this;
			xobj1.overrideMimeType("application/json");
			xobj1.open('GET', this.legs, true);
			xobj1.onreadystatechange = function () {
				if (xobj1.readyState == 4 && xobj1.status == "200") {
					// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
					this.caller.on_legs_loaded(xobj1.responseText);
				} else if ( xobj1.status != "200" ) {
					//this.caller.json_load_failed(xobj1);
				}
			}
			xobj1.send(null);
		}

	};
	
	this.on_morph_loaded = function( e ) {
	
		this.morph_data = JSON.parse( e );
		if ( this.legs == undefined || this.legs_data != undefined ) {
			this.on_data_loaded();
		}
	
	}
	
	this.on_legs_loaded = function( e ) {
	
		this.legs_data = JSON.parse( e );
		if ( this.morph_data != undefined ) {
			this.on_data_loaded();
		}
	
	}
	
	this.orientation_rotation_conversion = function( p, r, corrmat, extra_quat ) {
		
		var pos = new THREE.Vector3( p[0], p[1], p[2] );
		if ( extra_quat !== undefined && extra_quat['scl'] !== undefined ) {
			pos = pos.multiply( extra_quat['scl'] );
		}
		if ( extra_quat !== undefined && extra_quat['pos'] !== undefined ) {
			pos = pos.add( extra_quat['pos'] );
		}
		var quat = new THREE.Quaternion().setFromEuler( new THREE.Euler( r[0], r[1], r[2], 'XYZ' ) );
		if ( extra_quat !== undefined && extra_quat['rot'] !== undefined ) {
			quat = quat.multiply( extra_quat['rot'] );
		}
		var scale = new THREE.Vector3(1,1,1);
		var mat = new THREE.Matrix4().compose ( pos, quat, scale );
		mat = corrmat.clone().multiply( mat );
		mat.decompose ( pos, quat, scale );
		
		return [ pos, quat ];
		
	}
	
	this.on_data_loaded = function() {

		//var data = JSON.parse( e );
		
		var corrmat = new THREE.Matrix4().compose ( 
			this.correction_translation, 
			this.correction_rotation, 
			this.correction_scale );
		
		var rotmat = new THREE.Matrix4();
		rotmat.set( 
			1, 0, 0, 0,
			0, 0, 1, 0,
			0, -1, 0, 0,
			0, 0, 0, 1 );
				
		var km = Object.keys(this.morph_data.morphs);
		this.morph_keys = [];
		for ( var k in km ) {
			var i = parseInt( km[k] );
			if ( i !== i ) {
				console.log( "Wrong type of key!" );
			} else {
				if ( i > this.total_frames ) {
					this.total_frames = i;
				}
				this.morph_keys.push( km[k] );
			}
		}
		this.morph_keys.sort();
		
		if ( this.legs_data != undefined ) {
			
			this.bones_rotations = {};
			this.bones_positions = {};
			
			var p = new THREE.Vector3();
			var q = new THREE.Quaternion();
			var s = new THREE.Vector3();
			var mat;
			
			for ( var i in this.legs_data.anim_legs ) {
				
				var frame = this.legs_data.anim_legs[ i ];
				var frame_num = frame.frame;
				
				for ( var b in frame.bones ) {
					
					if ( frame.bones[b].quat ) {
						
						if ( this.bones_rotations[ b ] == undefined ) {
							//console.log( b );
							this.bones_rotations[ b ] = {};
							this.bones_rotations[ b ]['current'] = new THREE.Quaternion(0,0,0,1);
						}

						var qdata = frame.bones[b].quat;
						q = new THREE.Quaternion( qdata[0], qdata[1], qdata[2], qdata[3] );

						mat = new THREE.Matrix4();
						mat.makeRotationFromQuaternion( q );
						mat = rotmat.clone().multiply( mat );
						mat.decompose( p, q, s );
						q = this.adjustment_rotation.clone().multiply( q );
						this.bones_rotations[ b ][ frame_num ] = q.clone();
						
					}
					
					if ( frame.bones[b].pos ) {
						
						if ( this.bones_positions[ b ] == undefined ) {
							//console.log( b );
							this.bones_positions[ b ] = {};
							this.bones_positions[ b ]['current'] = new THREE.Vector3(0,0,0);
						}
						
						var pdata = frame.bones[b].pos;
						mat = new THREE.Matrix4().makeTranslation( pdata[0], pdata[1], pdata[2] );
						mat = rotmat.clone().multiply( mat );
						mat.decompose( p, q, s );
						this.bones_positions[ b ][ frame_num ] = p.multiplyScalar( this.correction_scale_scalar ).clone();
						
					}
					
				}
				
				//console.log( frame );
				
			}
			
			//console.log( this.bones_rotations );
			
		} else {
		
			this.trackers = {};
			var tracker_geom = new THREE.BoxGeometry( 0.02, 0.1, 0.02 );
			var tracker_mat0 = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
			var tracker_mat1 = new THREE.MeshBasicMaterial( {color: 0xff0000} );

			for ( k in this.morph_data.trackers ) {

				this.trackers[k] = {};

				this.trackers[k]['keys'] = {};
				this.trackers[k]['keys'][0] = this.orientation_rotation_conversion( 
					this.morph_data.trackers[k].position, 
					this.morph_data.trackers[k].eulers, 
					corrmat, 
					this.tracker_rotation[k] );

				this.trackers[k]['obj'] = new THREE.Object3D();
				this.scene.add( this.trackers[k]['obj'] );
	/*
	if ( k == 'tracker_foot_left' ) {
	this.trackers[k]['debug'] = new THREE.Mesh( tracker_geom, tracker_mat1 );
	} else {
	this.trackers[k]['debug'] = new THREE.Mesh( tracker_geom, tracker_mat0 );
	}
	this.trackers[k]['obj'].add( this.trackers[k]['debug'] );
	*/
				this.trackers[k]['obj'].position.copy( this.trackers[k]['keys'][0][0] );
				this.trackers[k]['obj'].quaternion.copy( this.trackers[k]['keys'][0][1] );

				for ( mk in this.morph_keys ) {
					this.trackers[k]['keys'][ parseInt( this.morph_keys[mk] ) ] = this.orientation_rotation_conversion( 
						this.morph_data.morphs[ this.morph_keys[mk] ].trackers[ k ].position, 
						this.morph_data.morphs[ this.morph_keys[mk] ].trackers[ k ].eulers, 
						corrmat, 
						this.tracker_rotation[k] );
				}
			}
			
		}

		// debugging of loaded trackers:
		/*
		for ( var k in this.trackers ) {
			var tracker = this.trackers[k];
			var tkeys = Object.keys(tracker['keys']);
			for( var tk in tkeys) {
				var obj3d = new THREE.Object3D();
				this.scene.add( obj3d );
				obj3d.add( Basics.debug_axis() );
				obj3d.position.copy( tracker['keys'][ tkeys[tk] ][0] );
				obj3d.quaternion.copy( tracker['keys'][ tkeys[tk] ][1] );
				if ( k != "tracker_center" ) {
					obj3d.scale.set( 0.5,0.5,0.5 );
				} else {
					obj3d.scale.set( 0.8,0.8,0.8 );
				}
			}
		}
		*/

		//console.log( this.total_frames );
		//console.log( this.trackers );

		function disposeArray() { this.array = null; }

		var has_morphs = false;
		var has_vnormals = false;
		
		var buffgeom = new THREE.BufferGeometry();
		buffgeom.setIndex( this.morph_data.indices );
		buffgeom.addAttribute( 'position', new THREE.Float32BufferAttribute( this.morph_data.vertices, 3 ).onUpload( disposeArray ) );
		if ( this.morph_data.normals !== undefined && this.morph_data.normals.length != 0 ) {
			buffgeom.addAttribute( 'normal', new THREE.Float32BufferAttribute( this.morph_data.normals, 3 ).onUpload( disposeArray ) );
		}
		if ( this.morph_data.colors !== undefined && this.morph_data.colors.length != 0 ) {
			//console.log( 'color loaded' );
			buffgeom.addAttribute( 'color', new THREE.Float32BufferAttribute( this.morph_data.colors, 3 ).onUpload( disposeArray ) );
		}
		if ( this.morph_data.uvs !== undefined && this.morph_data.uvs.length != 0 ) {
			//console.log( 'uv loaded' );
			buffgeom.addAttribute( 'uv', new THREE.Float32BufferAttribute( this.morph_data.uvs, 2 ).onUpload( disposeArray ) );
		}
		buffgeom.computeBoundingSphere();

		var geom = new THREE.Geometry();
		geom.fromBufferGeometry( buffgeom );

		this.maximum_frame = 0;
		
		for ( mk in this.morph_keys ) {
		
			if ( this.maximum_frame < this.morph_data.morphs[this.morph_keys[mk]].frame ) {
				this.maximum_frame = this.morph_data.morphs[this.morph_keys[mk]].frame;
			}
			
			var vs = this.morph_data.morphs[this.morph_keys[mk]].vertices;
			//var vn = this.morph_data.morphs[this.morph_keys[mk]].vertex_normals;
			var vn = this.morph_data.morphs[this.morph_keys[mk]].normals;
			var l = vs.length;
			var i = 0;

			has_morphs = true;
			has_vnormals = ( vn !== undefined && vn.length == this.morph_data.vertices.length );

			var frame = {
				name: parseInt( this.morph_keys[mk] ),
				vertices: []
			};

			if ( has_vnormals ) {
				frame.normals = [];
			}

			while ( i < l ) {
				frame.vertices.push( new THREE.Vector3( vs[i+0], vs[i+1], vs[i+2] ) );
				if ( has_vnormals ) {
					frame.normals.push( new THREE.Vector3( vn[i+0], vn[i+1], vn[i+2] ) );
				}
				i += 3;
			}

			geom.morphTargets.push( frame );
			
			if ( has_vnormals ) {

				geom.vertices = geom.morphTargets[ 0 ].vertices;
				var morphTarget = geom.morphTargets[ 0 ];
				for ( var j = 0, jl = geom.faces.length; j < jl; j ++ ) {
					var face = geom.faces[ j ];
					face.vertexNormals = [
						morphTarget.normals[ face.a ],
						morphTarget.normals[ face.b ],
						morphTarget.normals[ face.c ]
					];
				}

				for ( var i = 0, l = geom.morphTargets.length; i < l; i ++ ) {
					var morphTarget = geom.morphTargets[ i ];
					var vertexNormals = [];
					for ( var j = 0, jl = geom.faces.length; j < jl; j ++ ) {
						var face = geom.faces[ j ];
						vertexNormals.push( {
							a: morphTarget.normals[ face.a ],
							b: morphTarget.normals[ face.b ],
							c: morphTarget.normals[ face.c ]
						} );
					}
					geom.morphNormals.push( { vertexNormals: vertexNormals } );
				}

			}
			
		}

		// pushing
		/*
		var material = new THREE.MeshPhongMaterial( {
				color: 0xffffff,
				shininess: 100,
				morphTargets: has_morphs,
				morphNormals: has_vnormals } );
		var material = new THREE.MeshNormalMaterial( {
				morphTargets: has_morphs,
				morphNormals: has_vnormals } );
		*/
		
		var diffuse_text= this.path.substring(0, this.path.indexOf("_morph") ) + "_diffuse.png";
		
		var material = new THREE.MeshPhongMaterial( {
				map: new THREE.ImageUtils.loadTexture( diffuse_text ),
				morphTargets: has_morphs,
				morphNormals: has_vnormals } );

		this.obj3d = new THREE.Mesh( geom, material );
		this.obj3d.castShadow = SHADOW_ENABLED;
		this.obj3d.receiveShadow = SHADOW_ENABLED;
		this.obj3d.visible = this.default_visibility;
		
		this.scene.add( this.obj3d );

		this.obj3d.applyMatrix( corrmat );

		// converting morph_keys to ints
		var ks = this.morph_keys;
		this.morph_keys = [];
		for ( var k in ks ) {
			this.morph_keys.push( parseInt( ks[k] ) );
		}
		//console.log( this.morph_keys );

		this.ready = true;

		if ( this.ready_callbak !== undefined ) {
			this.ready_callbak();
		}

	};
	
	this.json_load_success = function( e ) {

		var data = JSON.parse( e );

		var corrmat = new THREE.Matrix4().compose ( 
			this.correction_translation, 
			this.correction_rotation, 
			this.correction_scale );

		//console.log( "\t!!!!! > " + this.path + " loaded successfully!" );

		var km = Object.keys(data.morphs);
		this.morph_keys = [];
		for ( var k in km ) {
			var i = parseInt( km[k] );
			if ( i !== i ) {
				console.log( "Wrong type of key!" );
			} else {
				if ( i > this.total_frames ) {
					this.total_frames = i;
				}
				this.morph_keys.push( km[k] );
			}
		}
		this.morph_keys.sort();

		this.trackers = {};

		var tracker_geom = new THREE.BoxGeometry( 0.02, 0.1, 0.02 );
		var tracker_mat0 = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
		var tracker_mat1 = new THREE.MeshBasicMaterial( {color: 0xff0000} );

		for ( k in data.trackers ) {
			
			this.trackers[k] = {};

			this.trackers[k]['keys'] = {};
			this.trackers[k]['keys'][0] = this.orientation_rotation_conversion( 
				data.trackers[k].position, 
				data.trackers[k].eulers, 
				corrmat, 
				this.tracker_rotation[k] );
			
			this.trackers[k]['obj'] = new THREE.Object3D();
			this.scene.add( this.trackers[k]['obj'] );
/*
if ( k == 'tracker_foot_left' ) {
this.trackers[k]['debug'] = new THREE.Mesh( tracker_geom, tracker_mat1 );
} else {
this.trackers[k]['debug'] = new THREE.Mesh( tracker_geom, tracker_mat0 );
}
this.trackers[k]['obj'].add( this.trackers[k]['debug'] );
*/
			this.trackers[k]['obj'].position.copy( this.trackers[k]['keys'][0][0] );
			this.trackers[k]['obj'].quaternion.copy( this.trackers[k]['keys'][0][1] );

			for ( mk in this.morph_keys ) {
				this.trackers[k]['keys'][ parseInt( this.morph_keys[mk] ) ] = this.orientation_rotation_conversion( 
					data.morphs[ this.morph_keys[mk] ].trackers[ k ].position, 
					data.morphs[ this.morph_keys[mk] ].trackers[ k ].eulers, 
					corrmat, 
					this.tracker_rotation[k] );
			}
		}

		// debugging of loaded trackers:
		/*
		for ( var k in this.trackers ) {
			var tracker = this.trackers[k];
			var tkeys = Object.keys(tracker['keys']);
			for( var tk in tkeys) {
				var obj3d = new THREE.Object3D();
				this.scene.add( obj3d );
				obj3d.add( Basics.debug_axis() );
				obj3d.position.copy( tracker['keys'][ tkeys[tk] ][0] );
				obj3d.quaternion.copy( tracker['keys'][ tkeys[tk] ][1] );
				if ( k != "tracker_center" ) {
					obj3d.scale.set( 0.5,0.5,0.5 );
				} else {
					obj3d.scale.set( 0.8,0.8,0.8 );
				}
			}
		}
		*/

		//console.log( this.total_frames );
		//console.log( this.trackers );

		function disposeArray() { this.array = null; }

		var has_morphs = false;
		var has_vnormals = false;
		
		var buffgeom = new THREE.BufferGeometry();
		buffgeom.setIndex( data.indices );
		buffgeom.addAttribute( 'position', new THREE.Float32BufferAttribute( data.vertices, 3 ).onUpload( disposeArray ) );
		if ( data.normals !== undefined && data.normals.length != 0 ) {
			buffgeom.addAttribute( 'normal', new THREE.Float32BufferAttribute( data.normals, 3 ).onUpload( disposeArray ) );
		}
		if ( data.colors !== undefined && data.colors.length != 0 ) {
			//console.log( 'color loaded' );
			buffgeom.addAttribute( 'color', new THREE.Float32BufferAttribute( data.colors, 3 ).onUpload( disposeArray ) );
		}
		if ( data.uvs !== undefined && data.uvs.length != 0 ) {
			//console.log( 'uv loaded' );
			buffgeom.addAttribute( 'uv', new THREE.Float32BufferAttribute( data.uvs, 2 ).onUpload( disposeArray ) );
		}
		buffgeom.computeBoundingSphere();

		var geom = new THREE.Geometry();
		geom.fromBufferGeometry( buffgeom );

		this.maximum_frame = 0;
		
		for ( mk in this.morph_keys ) {
		
			if ( this.maximum_frame < data.morphs[this.morph_keys[mk]].frame ) {
				this.maximum_frame = data.morphs[this.morph_keys[mk]].frame;
			}
			
			var vs = data.morphs[this.morph_keys[mk]].vertices;
			//var vn = data.morphs[this.morph_keys[mk]].vertex_normals;
			var vn = data.morphs[this.morph_keys[mk]].normals;
			var l = vs.length;
			var i = 0;

			has_morphs = true;
			has_vnormals = ( vn !== undefined && vn.length == data.vertices.length );

			var frame = {
				name: parseInt( this.morph_keys[mk] ),
				vertices: []
			};

			if ( has_vnormals ) {
				frame.normals = [];
			}

			while ( i < l ) {
				frame.vertices.push( new THREE.Vector3( vs[i+0], vs[i+1], vs[i+2] ) );
				if ( has_vnormals ) {
					frame.normals.push( new THREE.Vector3( vn[i+0], vn[i+1], vn[i+2] ) );
				}
				i += 3;
			}

			geom.morphTargets.push( frame );
			
			if ( has_vnormals ) {

				geom.vertices = geom.morphTargets[ 0 ].vertices;
				var morphTarget = geom.morphTargets[ 0 ];
				for ( var j = 0, jl = geom.faces.length; j < jl; j ++ ) {
					var face = geom.faces[ j ];
					face.vertexNormals = [
						morphTarget.normals[ face.a ],
						morphTarget.normals[ face.b ],
						morphTarget.normals[ face.c ]
					];
				}

				for ( var i = 0, l = geom.morphTargets.length; i < l; i ++ ) {
					var morphTarget = geom.morphTargets[ i ];
					var vertexNormals = [];
					for ( var j = 0, jl = geom.faces.length; j < jl; j ++ ) {
						var face = geom.faces[ j ];
						vertexNormals.push( {
							a: morphTarget.normals[ face.a ],
							b: morphTarget.normals[ face.b ],
							c: morphTarget.normals[ face.c ]
						} );
					}
					geom.morphNormals.push( { vertexNormals: vertexNormals } );
				}

			}
			
		}

		// pushing
		/*
		var material = new THREE.MeshPhongMaterial( {
				color: 0xffffff,
				shininess: 100,
				morphTargets: has_morphs,
				morphNormals: has_vnormals } );
		var material = new THREE.MeshNormalMaterial( {
				morphTargets: has_morphs,
				morphNormals: has_vnormals } );
		*/
		
		var diffuse_text= this.path.substring(0, this.path.indexOf("_morph") ) + "_diffuse.png";
		
		var material = new THREE.MeshPhongMaterial( {
				map: new THREE.ImageUtils.loadTexture( diffuse_text ),
				morphTargets: has_morphs,
				morphNormals: has_vnormals } );

		this.obj3d = new THREE.Mesh( geom, material );
		this.obj3d.castShadow = SHADOW_ENABLED;
		this.obj3d.receiveShadow = SHADOW_ENABLED;
		this.obj3d.visible = this.default_visibility;
		
		this.scene.add( this.obj3d );

		this.obj3d.applyMatrix( corrmat );

		// converting morph_keys to ints
		var ks = this.morph_keys;
		this.morph_keys = [];
		for ( var k in ks ) {
			this.morph_keys.push( parseInt( ks[k] ) );
		}
		//console.log( this.morph_keys );

		this.ready = true;

		if ( this.ready_callbak !== undefined ) {
			this.ready_callbak();
		}

	};
	
	this.update = function( deltatime ) {
		
		if ( !this.ready ) {
			return;
		}
		
		if ( this.playing ) {
			this.current_frame += deltatime * this.speed_frame;
		}
		while ( this.current_frame >= this.total_frames ) {
			this.current_frame -= this.total_frames;
		}
		
		this.percent = this.current_frame / this.total_frames;
		
		// seeking the closest keyframe
		var key_range = [0,-1];
		var key_range_ids = [0,0];
		for ( var k in this.morph_keys ) {
			if ( this.current_frame >= this.morph_keys[k] ) {
				key_range[0] = this.morph_keys[k];
				key_range_ids[0] = k;
			} else if ( key_range[1] == -1 ) {
				key_range[1] = this.morph_keys[k];
				key_range_ids[1] = k;
				break;
			}
		}
		
		var key_pc = ( this.current_frame - key_range[0] ) / ( key_range[1] - key_range[0] );
		
		for ( var b in this.bones_rotations ) {
			var boner = this.bones_rotations[b];
			var qstart = boner[key_range[0]].clone().slerp( boner[key_range[1]], key_pc );
			boner.current.set( qstart.x, qstart.y, qstart.z, qstart.w );
		}
		
		for ( var b in this.bones_positions ) {
			var boner = this.bones_positions[b];
			var qstart = boner[key_range[0]].clone().lerp( boner[key_range[1]], key_pc );
			boner.current.set( qstart.x, qstart.y, qstart.z );
		}
		
		for ( var k in this.trackers ) {
			var tracker = this.trackers[k];
			var tkeys = Object.keys(tracker['keys']);
			var p = tracker['keys'][key_range[0]][0].clone();
			var q = tracker['keys'][key_range[0]][1].clone();
			p.lerp( tracker['keys'][key_range[1]][0], key_pc );
			q.slerp( tracker['keys'][key_range[1]][1], key_pc );
			this.trackers[k]['obj'].position.copy( p );
			this.trackers[k]['obj'].quaternion.copy( q );
		}
		
		for ( var mti in this.obj3d.morphTargetInfluences ) {
			if ( mti != key_range_ids[0] && mti != key_range_ids[1] ) {
				this.obj3d.morphTargetInfluences[ mti ] = 0;
			} else if ( mti == key_range_ids[0] ) {
				this.obj3d.morphTargetInfluences[ mti ] = (1-key_pc);
			} else {
				this.obj3d.morphTargetInfluences[ mti ] = key_pc;
			}
		}
		
	}
	
}