/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

function GroundConfig() {

	this.rows = 0;
	this.columns = 0;
	this.color1 = new THREE.Color( 0x222222 );
	this.color2 = new THREE.Color( 0xffffff );
	this.emissive1 = new THREE.Color( 0x000000 );
	this.emissive2 = new THREE.Color( 0x444444 );
	this.size = new THREE.Vector3(1,1,1);
	this.use_cubes = true;
	this.normalmap_path = undefined;
	this.ctrlmap_path = undefined;
	this.ready_callbak = undefined;

	this.clone = function() {
		var out = new GroundConfig();
		out.rows = this.rows;
		out.columns = this.columns;
		out.color1.copy( this.color1 );
		out.color2.copy( this.color2 );
		out.emissive1.copy( this.emissive1 );
		out.emissive2.copy( this.emissive2 );
		out.size.copy( this.size );
		out.use_cubes = this.use_cubes;
		out.normalmap_path = this.normalmap_path;
		out.ctrlmap_path = this.ctrlmap_path;
		out.ready_callbak = this.ready_callbak;
		return out;
	};

};

function Ground() {

	this.obj3d = undefined;
	this.mat1 = undefined;
	this.mat2 = undefined;
	this.config = undefined;
	this.scene = undefined;
	this.ready = false;

	// utilities

	this.move = function( x, y, z ) {

		if ( this.ready === false ) return;
		this.obj3d.position.set(x, y, z);

	};

	this.scale = function( x, y, z ) {

		if ( this.ready === false ) return;
		this.obj3d.set(x, y, z);

	};

	this.apply_normalmap = function( tex ) {

		if ( !this.ready ) {
			return;
		}

		this.mat1.normalMap = tex;
		this.mat2.normalMap = tex;
		this.mat1.needsUpdate = true;
		this.mat2.needsUpdate = true;

	};

	this.apply_ctrlmap = function( tex ) {

		if ( !this.ready ) {
			return;
		}

		this.mat1.aoMap = tex; // red
		this.mat2.aoMap = tex;
		this.mat1.roughnessMap = tex; // green
		this.mat2.roughnessMap = tex;
		this.mat1.metalnessMap = tex; // blue
		this.mat2.metalnessMap = tex;
		this.mat1.needsUpdate = true;
		this.mat2.needsUpdate = true;

	};

	this.configure = function( config ) {

		this.config = config.clone();

	};

	this.load = function( scene ) {

		if ( this.config === undefined ) {
			console.log( "You have to configure this Ground before loading it!" );
			console.log( "Create a GroundConfig and pass it to 'configure()' " );
			return;
		}

		this.scene = scene;

		this.obj3d = new THREE.Object3D();
		this.scene.add( this.obj3d );

		this.mat1 = new THREE.MeshPhongMaterial({
			color: this.config.color1,
			emissive: this.config.emissive1,
			roughness: 0.1,
			normalScale: new THREE.Vector2( 1,1 )
		});

		this.mat2 = this.mat1.clone();
		this.mat2.color = this.config.color2;
		this.mat2.emissive = this.config.emissive2;
		
		if (typeof GROUND_PURE_BLACK !== undefined ) {
			this.mat1.reflectivity = 0;
			this.mat1.shininess = 0;
			this.mat1.roughness = 0;
			this.mat1.color = new THREE.Color( 0x000000 );
			this.mat1.emissive = new THREE.Color( 0x000000 );
			this.mat1.specular = new THREE.Color( 0x000000 );
		}
		
		var cgeom = undefined;
		if ( this.config.use_cubes ) {
			cgeom = new THREE.BoxGeometry(
					this.config.size.x,
					this.config.size.y,
					this.config.size.z
				);
		} else {
			cgeom = new THREE.PlaneGeometry(
					this.config.size.x,
					this.config.size.z
				);
		}

		var offset = new THREE.Vector3(
				this.config.size.x * ( this.config.columns - 1 ) * -0.5,
				this.config.size.y * -0.5,
				this.config.size.z * ( this.config.rows - 1 ) * -0.5
		);

		var current = new THREE.Vector3().copy( offset );

		for( var r = 0; r < this.config.rows; ++r ) {
			var switcher = 0;
			if ( r%2 == 0 ) {
				switcher = 1;
			}
			current.x = offset.x;
			for( var c = 0; c < this.config.columns; ++c ) {
				var m = this.mat1;
				if ( switcher == 0 ) {
					 m = this.mat2;
				}
				var cell = new THREE.Mesh( cgeom, m );
				cell.castShadow = false;
				cell.receiveShadow = SHADOW_ENABLED;
				this.obj3d.add( cell );
				cell.position.copy( current );
				if ( this.config.use_cubes ) {
					var rand = Math.random();
					if ( rand > 0.75 ) {
						cell.rotateY( Math.PI * 0.5 );
					} else if ( rand > 0.5 ) {
						cell.rotateY( Math.PI );
					} else if ( rand > 0.25 ) {
						cell.rotateY( Math.PI * 1.5 );
					}
				} else {
					cell.rotateX( Math.PI * -0.5 );
				}
				switcher = ( switcher + 1 ) % 2;
				current.x += this.config.size.x;
			}
			current.z += this.config.size.z;
		}

		this.on_loading_complete();

		if ( this.config.normalmap_path !== undefined ) {
			this.load_normalmap();
		}
		if ( this.config.ctrlmap_path !== undefined ) {
			this.load_ctrlmap();
		}

	};

	this.load_normalmap = function() {

		var loader = new THREE.TextureLoader();
		loader.texture_path = this.config.normalmap_path;
		loader.caller = this;
		loader.load(
			loader.texture_path,
			function ( texture ) {
				loader.caller.apply_normalmap( texture );
			},
			// onProgress callback currently not supported
			undefined,
			// onError callback
			function ( err ) {
				console.error( 'An error happened.' );
			}
		);

	};

	this.load_ctrlmap = function() {

		var loader = new THREE.TextureLoader();
		loader.texture_path = this.config.ctrlmap_path;
		loader.caller = this;
		loader.load(
			loader.texture_path,
			function ( texture ) {
				loader.caller.apply_ctrlmap( texture );
			},
			// onProgress callback currently not supported
			undefined,
			// onError callback
			function ( err ) {
				console.error( 'An error happened.' );
			}
		);

	};

	this.on_loading_complete = function() {

		if ( !this.ready ) {
			this.ready = true;
			if ( this.config.ready_callbak !== undefined ) {
				this.config.ready_callbak();
			}
		}

	};

};
