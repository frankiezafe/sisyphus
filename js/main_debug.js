/*
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2018 polymorph.cool
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

// *********************************
// ************ CONFIG *************
// *********************************

var config_sysiphus_avatar = new SysiphusAvatarConfig();
config_sysiphus_avatar.mesh = "Body_Idle";
config_sysiphus_avatar.skeleton_root = "Armature_breathing";
config_sysiphus_avatar.path_model = 'models/geraldine.dae';
config_sysiphus_avatar.path_animations = [
	'models/geraldine_idle.dae',
	'models/geraldine_sleeping.dae',
	'models/geraldine_sleeping_back.dae',
	'models/geraldine_jump_back.dae'];
config_sysiphus_avatar.name_animations = [
	"idle",
	"sleeping side",
	"sleeping back",
	"jumping back"];
config_sysiphus_avatar.name_default_animation = "idle";
config_sysiphus_avatar.path_deforms = ['presets/deform_test.json','presets/deform_test2.json'];
config_sysiphus_avatar.ready_callbak = on_avatar_ready;

var config_pantonpouf = new PantonPoufConfig();
config_pantonpouf.morph_path = 'models/pouf.json';
config_pantonpouf.morph_default = 'basis';
config_pantonpouf.butt_anim_path = 'models/butt_anims.json';
config_pantonpouf.left_foot_anchor = { 'pos': new THREE.Vector3( 0.24,1.07,0.13 ), 'euler': new THREE.Euler( -0.95,0,Math.PI, 'XYZ' ) };
config_pantonpouf.right_foot_anchor = { 'pos': new THREE.Vector3( -0.24,1.07,0.13 ), 'euler': new THREE.Euler( -0.95,0,Math.PI, 'XYZ' ) };
config_pantonpouf.weight_speed = 2.5;
config_pantonpouf.ready_callbak = on_pouf_ready;

// *********************************
// ************ GLOBALS ************
// *********************************

var container, stats, clock, controls;
var camera, scene, renderer;

var gridHelper = undefined;

var SCENE_LOADED = false;

var avatar = undefined;

// debug objects

var axis_3d = undefined;
var frame_count = 0;

// animation controls

var track_head_weight = Interpolation.new_interpolation( "test", 0, 2,  Interpolation.sin );
var track_head = undefined;
var track_head_list = ["__NONE__","camera"];
var track_head_current = track_head_list[0];
var track_head_targets = undefined;

var stats_enabled = undefined;

// *********************************
// *********** FUNCTIONS ***********
// *********************************

init();

animate();

function init() {

	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.position.set( 5, 2.5, 5 );
	scene = new THREE.Scene();
	clock = new THREE.Clock();

	gridHelper = new THREE.GridHelper( 10, 20 );
	scene.add( gridHelper );

	// lightning
	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.2 );
	scene.add( ambientLight );
	var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.8 );
	directionalLight.position.set( 1, 1, 1 );
	scene.add( directionalLight );

	// renderer
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor( new THREE.Color( 0x242424 ), 1 );
	container.appendChild( renderer.domElement );

	// camera controls
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 2, 0 );
	controls.update();

	// debug
	var geometry;
	var material;
	var wireframe;
	
	axis_3d = new THREE.Object3D();
		// axis
		var cylinder;
		var scale = 0.3;
		// X axis
		geometry = new THREE.CylinderGeometry( 0.015, 0.015, 1, 3 );
		material = new THREE.MeshBasicMaterial( {color: 0xff0000} );
		cylinder = new THREE.Mesh( geometry, material );
		axis_3d.add( cylinder );
		cylinder.position.set( scale / 2, 0, 0 );
		cylinder.scale.set( scale,scale,scale );
		cylinder.rotation.set( 0, 0, Math.PI * 0.5 );
		// Y axis
		material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
		cylinder = new THREE.Mesh( geometry, material );
		axis_3d.add( cylinder );
		cylinder.position.set( 0, scale / 2, 0 );
		cylinder.scale.set( scale,scale,scale );
		cylinder.rotation.set( 0, 0, 0 );
		// Z axis
		material = new THREE.MeshBasicMaterial( {color: 0x0000ff} );
		cylinder = new THREE.Mesh( geometry, material );
		axis_3d.add( cylinder );
		cylinder.position.set( 0, 0, scale / 2 );
		cylinder.scale.set( scale,scale,scale );
		cylinder.rotation.set( Math.PI * 0.5, 0, 0 );

	track_head = new THREE.Object3D();
	scene.add( track_head );

	// avatar
	avatar = new SysiphusAvatar();
	avatar.configure( config_sysiphus_avatar );
	avatar.load( scene );

	pouf = new PantonPouf();
	pouf.configure( config_pantonpouf );
	pouf.load( scene );

	gridHelper.position.set( 0, 1, 0 );

	if ( stats_enabled !== undefined ) {
		stats = new Stats();
		container.appendChild( stats.dom );
	}

	// events
	window.addEventListener( 'resize', onWindowResize, false );

}

function on_avatar_ready() {

	avatar.move( 0, 1, 0.325 );
	// axis_3d_free.position.set( 0, 1.5, 0 );
	// console.log( avatar.obj3d_mesh.material );
	// avatar.obj3d_mesh.material.color = new THREE.Color( 0x000000 );
	// avatar.obj3d_mesh.material.specular = new THREE.Color( 0x660033 );
	// avatar.obj3d_mesh.material.emissive = new THREE.Color( 0x220022 );
	avatar.obj3d_mesh.material.shininess = 2;
	// avatar.obj3d_mesh.material.reflectivity = 0;
	// avatar.obj3d_mesh.material.vertexColors = new THREE.Color( 0xff00ff );
	// avatar.obj3d_mesh.material.wireframe = true;
	// avatar.obj3d_mesh.visible = false;
	// initialise track_head_targetss
	track_head_targets = {};
	for ( var i in track_head_list ) {
		var name = track_head_list[i];
		if ( name == "camera" ) {
			track_head_targets[name] = [ camera.position, 0 ];
		} else {
			track_head_targets[name] = [ undefined, 0 ];
		}
	}

	if ( pouf.ready ) {
		on_all_ready();
	}

}

function on_pouf_ready() {

	pouf.move( 0, 1, -0.325 );
	pouf.scale( 0.3, 0.3, 0.3 );

	var loader = new THREE.TextureLoader();
	loader.load(
		'textures/grid.png',
		function ( texture ) {
			print( "texture 'textures/grid.png' loaded" );
			pouf.apply_texture( texture );
		},
		// onProgress callback currently not supported
		undefined,
		// onError callback
		function ( err ) {
			print( "texture 'textures/grid.png' loading failed" );
			console.error( 'An error happened.' );
		}
	);

	if ( avatar.ready ) {
		on_all_ready();
	}

}

function on_all_ready() {

	avatar.glue_to( "mixamorig_Hips", pouf.butt, 1 );
	avatar.ik( "mixamorig_LeftFoot", pouf.left_foot, 1 );
	avatar.ik( "mixamorig_RightFoot", pouf.right_foot, 1 );

	populate_info();

}

function number_display( num ) {

	var str = "" + Math.floor( num * 100 ) / 100;
	var n = str.indexOf(".");
	if ( n == -1 ) {
		return str + ".00";
	} else if  ( n == str.length - 2 ) {
		return str + "0";
	}
	return str;

}

function populate_info() {

	var row;
	var cell;

	var info_animations = document.getElementById( 'info_animations' );
	var info_animations_body = document.getElementById( 'info_animations_body' );
	for ( var i = 0; i < avatar.animations.length; ++i ) {
		var j = 0;
		var weight = avatar.mixer.clipAction( avatar.animations[i] ).getEffectiveWeight();
		// adding a row
		row = info_animations_body.insertRow( info_animations_body.rows.length );
		row.id = "info_" + avatar.animations[i].name;
		// adding cells
		// checkbox
		cell = row.insertCell(j); j++;
		cell.id = "info_radio_" + avatar.animations[i].name;
		cell.className = "radio first";
		var checkbox = document.createElement('input');
		checkbox.type = "radio";
		checkbox.name = "radio_animation_selector";
		checkbox.id = "radio_" + avatar.animations[i].name;
		checkbox.value = avatar.animations[i].name;
		checkbox.className = "radio_animation";
		checkbox.addEventListener( "click", function(){ avatar.request_anim = this.value; } );
		if ( avatar.animations[i].name == avatar.request_anim ) {
			checkbox.checked = true;
		}
		cell.appendChild( checkbox ) ;
		// name
		cell = row.insertCell(j); j++;
		cell.className = "name first";
		cell.appendChild(document.createTextNode( avatar.animations[i].name ));
		// duration
		cell = row.insertCell(j); j++;
		cell.className = "duration";
		cell.appendChild(document.createTextNode( number_display( avatar.animations[i].duration ) ));
		// time
		cell = row.insertCell(j); j++;
		cell.id = "info_time_" + avatar.animations[i].name;
		cell.className = "time";
		cell.appendChild(document.createTextNode( number_display( avatar.mixer.clipAction( avatar.animations[i] ).time ) ));
		// speed
		cell = row.insertCell(j); j++;
		cell.id = "info_speed_" + avatar.animations[i].name;
		cell.className = "speed";
		cell.appendChild(document.createTextNode( number_display( avatar.mixer.clipAction( avatar.animations[i] ).timeScale ) ));
		// weight
		cell = row.insertCell(j); j++;
		cell.id = "info_weight_" + avatar.animations[i].name;
		cell.className = "weight";
		cell.appendChild(document.createTextNode( number_display( weight ) ));
		// loop display
		cell = row.insertCell(j); j++;
		cell.id = "info_looped_" + avatar.animations[i].name;
		cell.className = "looped last";
		var checkbox = document.createElement('div');
		checkbox.className = "looped_checkbox";
		checkbox.id = "info_looped_checkbox_" + avatar.animations[i].name;
		cell.appendChild( checkbox );
	}

	var info_tracking = document.getElementById( 'info_tracking_head' );
	var info_tracking_body = document.getElementById( 'info_tracking_head_body' );
	for ( var i in track_head_list ) {
		var name = track_head_list[i];
		var j = 0;
		// adding a row
		row = info_tracking_body.insertRow(info_tracking_body.rows.length);
		row.id = "info_tracking_" + name;
		// adding cells

		// checkbox
		cell = row.insertCell(j); j++;
		cell.id = "info_tracking_head_" + name;
		cell.className = "radio first";
		var checkbox = document.createElement('input');
		checkbox.type = "radio";
		checkbox.name = "radio_tracking_head_selector";
		checkbox.id = "radio_" + name;
		checkbox.value = name;
		checkbox.className = "radio_animation";
		checkbox.addEventListener( "click", function(){ set_track_head( this.value ); } );
		if ( name == "__NONE__" ) {
			checkbox.checked = true;
		}
		cell.appendChild( checkbox ) ;
		// name
		cell = row.insertCell(j); j++;
		cell.className = "name";
		cell.appendChild(document.createTextNode( name ));
		// weight
		cell = row.insertCell(j); j++;
		cell.id = "info_tracking_weight_" + name;
		cell.className = "weight last";
		cell.appendChild(document.createTextNode( number_display( track_head_targets[name][1] ) ));
	}

	var info_pouf= document.getElementById( 'info_pouf_head' );
	var info_pouf_body = document.getElementById( 'info_pouf_body' );
	for ( var k in pouf.pouf.morphTargetDictionary ) {
		var name = k;
		var morph_id = pouf.pouf.morphTargetDictionary[k];
		var j = 0;
		// adding a row
		row = info_pouf_body.insertRow(info_pouf_body.rows.length);
		row.id = "info_morph_" + name;
		// adding cells
		// checkbox
		cell = row.insertCell(j); j++;
		cell.id = "info_morph_" + name;
		cell.className = "radio first";
		var checkbox = document.createElement('input');
		checkbox.type = "radio";
		checkbox.name = "radio_morph_selector";
		checkbox.id = "radio_" + name;
		checkbox.value = name;
		checkbox.className = "radio_morph";
		checkbox.addEventListener( "click", function(){ pouf.request_morph = this.value; } );
		if ( name == config_pantonpouf.default_morph ) {
			checkbox.checked = true;
		}
		cell.appendChild( checkbox ) ;
		// name
		cell = row.insertCell(j); j++;
		cell.className = "name";
		cell.appendChild(document.createTextNode( name ));
		// weight
		cell = row.insertCell(j); j++;
		cell.id = "info_morph_weight_" + name;
		cell.className = "weight last";
		cell.appendChild(document.createTextNode( number_display( pouf.get_morph_weight(name) ) ));
	}

	// var info_scales = document.getElementById( 'info_scale_presets' );
	// var info_scales_body = document.getElementById( 'info_scale_presets_body' );
	// for ( var name in avatar.deforms ) {
	// 	var j = 0;
	// 	// adding a row
	// 	row = info_scales_body.insertRow(info_scales_body.rows.length);
	// 	row.id = "info_scale_presets_" + name;
	// 	// adding cells
	// 	// checkbox
	// 	cell = row.insertCell(j); j++;
	// 	cell.id = "info_scale_presets_" + name;
	// 	cell.className = "radio first";
	// 	var checkbox = document.createElement('input');
	// 	checkbox.type = "radio";
	// 	checkbox.name = "radio_scale_presets_selector";
	// 	checkbox.id = "radio_" + name;
	// 	checkbox.value = name;
	// 	checkbox.className = "radio_animation";
	// 	checkbox.addEventListener( "click", function(){ avatar.goto_deform( this.value ); } );
	// 	if ( name == "__UNIT__" ) {
	// 		checkbox.checked = true;
	// 	}
	// 	cell.appendChild( checkbox ) ;
	// 	// name
	// 	cell = row.insertCell(j); j++;
	// 	cell.className = "name last";
	// 	cell.appendChild(document.createTextNode( name ));
	// }

	for( var b in avatar.bones ) {
	 	print( b );
	}

}

function update_info() {

	if ( avatar.ready === true ) {
		for ( var i = 0; i < avatar.animations.length; ++i ) {
			var aname = avatar.animations[i].name;
			var track = avatar.mixer.clipAction( avatar.animations[i] );
			document.getElementById( "info_weight_" + aname ).innerHTML = 
				number_display( track.getEffectiveWeight() );
			document.getElementById( "info_speed_" + aname ).innerHTML = 
				number_display( track.getEffectiveTimeScale() );
			document.getElementById( "info_time_" + aname ).innerHTML = 
				number_display( track.time );
			var cb = document.getElementById( "info_looped_checkbox_" + aname );
			if ( track.just_looped && cb.className != "looped_checkbox checked" ) {
				cb.className = "looped_checkbox checked";
			} else if ( cb.className == "looped_checkbox checked" ) {
				cb.className = "looped_checkbox normal";
			}
		}
		for ( var name in track_head_targets ) {
			document.getElementById( "info_tracking_weight_" + name ).innerHTML = 
				number_display( track_head_targets[name][1] );
		}
	}
	if ( pouf.ready === true ) {
		for ( var name in pouf.morph_weights ) {
			if ( pouf.morph_weights.idle ) { continue; }
			try {
				document.getElementById( "info_morph_weight_" + name ).innerHTML = 
					number_display( pouf.morph_weights[name].value );
			} catch( e ) {}
		}
	}

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

	requestAnimationFrame( animate );
	render();
	if ( stats_enabled !== undefined ) {
		stats.update();
	}

}

function render() {

	var delta = clock.getDelta();

	Interpolation.update( delta );

	var track_headp = track_head.position;
	for ( var name in track_head_targets ) {
		var target = track_head_targets[name];
		if ( track_head_current == name && target[1] < 1 ) {
			target[1] += delta;
			if ( target[1] > 1 ) {
				target[1] = 1;
			}
		} else if ( track_head_current != name && target[1] > 0 ) {
			target[1] -= delta;
			if ( target[1] < 0 ) {
				target[1] = 0;
			}
		}
		if ( target[0] !== undefined ) {
			track_headp.lerp( target[0], target[1] );
		}
	}

	track_head.updateMatrix();

	if ( avatar.ready === true && pouf.ready === true ) {

		if ( !track_head_weight.idle ) {
			var w = track_head_weight.value;
			if ( w == 0 ) {
				avatar.track_to( "mixamorig_Spine2", undefined );
				avatar.track_to( "mixamorig_Neck", undefined );
				avatar.track_to( "mixamorig_Head", undefined );
			} else {
				avatar.track_to( "mixamorig_Spine2", track_head, w * 0.4 );
				avatar.track_to( "mixamorig_Neck", track_head, w * 0.7 );
				avatar.track_to( "mixamorig_Head", track_head, w );
			}
		}

		avatar.update( delta );
		pouf.update( delta );

	}

	renderer.render( scene, camera );
	
	update_info();

	frame_count++;

}

function set_track_head( name ) {

	if ( name == track_head_list[0] ) {
		track_head_weight.target = 0;
	} else {
		track_head_weight.target = 1;
	}
	track_head_current = name;

}

function print( e ) {

	document.getElementById("debugger").innerHTML += e;
	document.getElementById("debugger").innerHTML += "<br/>";

}

function debug( e ) {

	document.getElementById("debugger").innerHTML = e;

}

function debug_json( e ) {

	document.getElementById("debugger").innerHTML = JSON.stringify(e, null, 4);

}