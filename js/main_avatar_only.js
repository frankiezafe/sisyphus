/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

if ( ! Detector.webgl ) {
	Detector.addGetWebGLMessage();
}

// *********************************
// ************ CONFIG *************
// *********************************

{

	var request_chairid = undefined;

	var SHADOW_ENABLED = true;
	var SHADOW_MAP_WIDTH = 2048, SHADOW_MAP_HEIGHT = 2048;
	
	// GROUND
	var config_ground = new GroundConfig();
	config_ground.rows = 25;
	config_ground.columns = 25;
	config_ground.size.set( 0.35, 0, 0.35 );
	config_ground.use_cubes = false;
	config_ground.ready_callbak = on_ground_ready;

	// NEW GERALDINE
	var config_sysiphus_avatar = new SysiphusAvatarConfig();
	config_sysiphus_avatar.mesh = "body";
	config_sysiphus_avatar.skeleton_root = "geraldine";
	config_sysiphus_avatar.path_model = 'models/geraldine2.dae';
	config_sysiphus_avatar.path_animations = [
		'models/geraldine2_idle.dae',
		'models/geraldine2_arms_crossed.dae',
		'models/geraldine2_baille.dae'
	];
	config_sysiphus_avatar.name_animations = [
		"idle",
		"arms_crossed",
		"baille"
	];
	config_sysiphus_avatar.name_default_animation = "idle";
	config_sysiphus_avatar.path_deforms = [];
	config_sysiphus_avatar.ready_callbak = on_avatar_ready;
	config_sysiphus_avatar.weight_speed = 0.5;
	config_sysiphus_avatar.debug_skeleton = true;
	config_sysiphus_avatar.diffuse_map = 'textures/VEN_Sisyphus_Texture-normal.png';
	config_sysiphus_avatar.specular_map = 'textures/avatar_specular.png';
	config_sysiphus_avatar.normal_map = 'textures/avatar_normal.png';

	sisyphus_cubes = [];
	sisyphus_cubes_json = 'models/generated/sisyphus_cubes.json';
	sisyphus_cubes_library = undefined;
	sisyphus_cubes_current_button = undefined;
	sisyphus_cubes_current_name = undefined;

	// *********************************
	// ************ GLOBALS ************
	// *********************************

	var container, stats, clock, controls;
	var camera, fps_camera, scene, renderer;
	var effectFXAA, bloomPass, renderScene;
	var composer;

	var SCENE_LOADED = false;
	var fps_camera_active = false;

	var sky;
	var ground = undefined;
	var avatar = undefined;
	var pouf = undefined;
	var cube = undefined;
	var info_panel_pivot = undefined;
	var tmngr = undefined;

	// debug objects

	var lightShadowMapViewer;

	var axis_3d = undefined;
	var frame_count = 0;

	var cube_to_load = undefined;
	var wait_to_load = 0;
	var wait_to_hide_loading = 0;

	// animation controls

	var head_tracker = undefined;

	var stats_enabled = false;
	var timeline_slider_enabled = false;

	var looking_min_delay = 5;
	var looking_max_delay = 50;
	var looking_duration = 10;
	var looking_timer = 0;
	var looking_enabled = false;

	var light_pivot = undefined;
	var ambient_light = undefined;
	var main_light = undefined;
	var second_light = undefined;

	var info_canvas = undefined;
	var info_canvas_context = undefined;
	var info_visible = false;

	var inventory_scrolltop = 0;

	var default_camera_position = new THREE.Vector3( 0, -0.4, 5 );

	var target_camera_position = undefined;
	var current_camera_position = undefined;
	var target_camera_target = new THREE.Vector3( 0, 0.75, 0 );
	var current_camera_target = undefined;

	var camera_position_transition_time = 1.0;
	var camera_position_transition_current = 0;

	var splash_finished = false;
	var close_splash_request = false;
	var menu_close_timer = -1;

}

// *********************************
// *********** FUNCTIONS ***********
// *********************************

function reset_camera() {
	camera.position.set( default_camera_position.x, default_camera_position.y, default_camera_position.z );
}

function init_controls() {
	
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 1, 0 );
	controls.enableZoom = false;
	controls.enablePan = false;
	controls.enableDamping = true;
	controls.dampingFactor = 0.1;
	// controls.minPolarAngle = Math.PI * -0.8;
	// controls.maxPolarAngle = Math.PI * 0.52;
	controls.rotateSpeed = .078;
	controls.update();
	
}

function init_shadow_viewer() {
	
	lightShadowMapViewer = new THREE.ShadowMapViewer( main_light );
	lightShadowMapViewer.position.x = 10;
	lightShadowMapViewer.position.y = window.innerHeight - 300;
	lightShadowMapViewer.size.width = 250;
	lightShadowMapViewer.size.height = 250;
	lightShadowMapViewer.update();
	
}

function init_threejs() {

	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 10000 );
	reset_camera();
	
	scene = new THREE.Scene();

	clock = new THREE.Clock();

	// lightning
	light_pivot = new THREE.Object3D();
	scene.add( light_pivot );

	ambient_light = new THREE.AmbientLight( 0xfff2ee, 0.32 );
	light_pivot.add( ambient_light );

	main_light = new THREE.DirectionalLight( 0xfdfdff, 0.9 );
	if ( SHADOW_ENABLED ) {
		main_light.castShadow = SHADOW_ENABLED;
		main_light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 25, 1, 2.5, 9 ) );
		main_light.shadow.bias = 0.0001;
		main_light.shadow.mapSize.width = SHADOW_MAP_WIDTH;
		main_light.shadow.mapSize.height = SHADOW_MAP_HEIGHT;
	}
	var pmult = 8;
	main_light.position.set( 0.4 * pmult, 0.5 * pmult, 0.2 * pmult );
	main_light.rotation.x = 1;
	light_pivot.add( main_light );
	
	init_shadow_viewer();
	
	second_light = new THREE.DirectionalLight( 0xfdfdff, 0.2 );
	second_light.position.set( -1.2, 0, 0.5 );
	light_pivot.add( second_light );

	// renderer
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor( new THREE.Color( 0x000000 ), 1 );
	if ( SHADOW_ENABLED ) {
		renderer.shadowMap.enabled = true;
		renderer.shadowMap.type = THREE.PCFShadowMap;
	}
	container.appendChild( renderer.domElement );

	// camera controls
	init_controls();
	
	track_head = new THREE.Object3D();
	scene.add( track_head );

	// ground
	ground = new Ground();
	ground.configure( config_ground );
	ground.load( scene );
	
	// avatar
	avatar = new SysiphusAvatar();
	avatar.configure( config_sysiphus_avatar );
	avatar.load( scene );
	
	cube = new Cube();
	cube.scene = scene;
	cube.ready_callbak = on_cube_ready;
	
	/*
	//cube.path = "models/sisyphus_cube_2018.09.13.14.15.01_morph.json";
	cube.path = sisyphus_cubes[11];
	cube.scene = scene;
	cube.ready_callbak = on_cube_ready;
	cube.load();
	*/

	head_tracker = new Tracker();
	head_tracker.init( 'head', scene );
	head_tracker.add_track( 'camera', camera.position );
	
	info_panel_pivot = new THREE.Object3D();
	scene.add( info_panel_pivot );
	
	var info_panel_mat = new THREE.MeshBasicMaterial( {color: 0xffffff, transparent: true } );
	var info_panel;
	info_panel = new THREE.Mesh( new THREE.PlaneBufferGeometry( 1.5, 1.5, 1.5 ), info_panel_mat );
	info_panel.receiveShadow = SHADOW_ENABLED;
	info_panel.rotation.set( Math.PI * 0.5, -Math.PI, Math.PI * 0.25 );
	info_panel.position.set( 0.4, 0.01, 0.4 );
	info_panel_pivot.add( info_panel );
	info_panel = new THREE.Mesh( new THREE.PlaneBufferGeometry( 1.5, 1.5, 1.5 ), info_panel_mat );
	info_panel.receiveShadow = SHADOW_ENABLED;
	info_panel.rotation.set( Math.PI * 0.5, -Math.PI, Math.PI * 1.25 );
	info_panel.position.set( -0.4, 0.01, -0.4 );
	info_panel_pivot.add( info_panel );
	
	info_panel_pivot.visible = false;
	
	
	if ( stats_enabled ) {
		stats = new Stats();
		container.appendChild( stats.dom );
	}

	// effects configuration
	scene.fog = new THREE.Fog( 0x000000, 3, 15 );

	// events
	window.addEventListener( 'resize', onWindowResize, false );
	
	// retrieval of cubes listing
	cube_listing_load();
	
	info_canvas_init();

}

function loadcube( c ) {

	cube.path = c.morph;
	cube.legs = c.legs;
	cube.invert_tracker = c.invert_tracker;
	cube.load();

}

function on_avatar_ready() {

	// attach the fps camera to the head
	fps_camera = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 0.13, 10000 );
	avatar.bones["mixamorig_Head"].bone.add( fps_camera );
	fps_camera.position.y = 0.15;
	fps_camera.position.z = 0.05;
	fps_camera.quaternion.set(0,0,0,1);
	fps_camera.rotateY( Math.PI );
	fps_camera.rotateX( -0.3 );
	
	main_light.target = avatar.bones["mixamorig_Spine"].bone;
	
	if ( splash_finished ) {
		close_splash();
	} else {
		close_splash_request = true;
	}
	
	if ( ground !== undefined && cube !== undefined && ground.ready && cube.ready ) {
		on_all_ready();
	}

}

function on_ground_ready() {

	ground.obj3d.rotateY( Math.PI * 0.25 );
	ground.obj3d.visible = true;

	if ( cube !== undefined && avatar !== undefined && cube.ready && avatar.ready ) {
		on_all_ready();
	}

}

function on_cube_ready() {
	
	if ( cube_to_load != undefined ) {
		cube_to_load = undefined;
		wait_to_hide_loading = 1;
	}
	
	if ( ground !== undefined && avatar !== undefined && ground.ready && avatar.ready ) {
		on_all_ready();
	}

}

function on_all_ready() {

	// initialise track_head_targets
	//head_tracker.add_track( 'pouf', pouf.morphable.obj3d );
	head_tracker.add_influence( avatar, 'mixamorig_Spine1', 0.1 );
	head_tracker.add_influence( avatar, 'mixamorig_Spine2', 0.4 );
	head_tracker.add_influence( avatar, 'mixamorig_Neck', 0.8 );
	head_tracker.add_influence( avatar, 'mixamorig_Head', 1 );
	head_tracker.track( "camera" );
	
	// automatic switch between control modes
	
	if ( cube.bones_rotations ) {
	
		avatar.copy_quat( "mixamorig_Hips", cube.bones_rotations["mixamorig:Hips"].current, 1 );
		avatar.copy_pos( "mixamorig_Hips", cube.bones_positions["mixamorig:Hips"].current, 1 );
		avatar.copy_quat( "mixamorig_RightUpLeg", cube.bones_rotations["mixamorig:RightUpLeg"].current, 1 );
		avatar.copy_quat( "mixamorig_RightLeg", cube.bones_rotations["mixamorig:RightLeg"].current, 1 );
		avatar.copy_quat( "mixamorig_RightFoot", cube.bones_rotations["mixamorig:RightFoot"].current, 1 );
		avatar.copy_quat( "mixamorig_LeftUpLeg", cube.bones_rotations["mixamorig:LeftUpLeg"].current, 1 );
		avatar.copy_quat( "mixamorig_LeftLeg", cube.bones_rotations["mixamorig:LeftLeg"].current, 1 );
		avatar.copy_quat( "mixamorig_LeftFoot", cube.bones_rotations["mixamorig:LeftFoot"].current, 1 );
		
	} else if ( cube.trackers ) {
		
		avatar.glue_to( "mixamorig_Hips", cube.trackers['tracker_center']['obj'], 1 );
		if ( cube.invert_tracker === true ) {
			console.log( "TRACKERS ARE INVERTED!" );
			avatar.ik( "mixamorig_RightFoot", cube.trackers['tracker_foot_right']['obj'], 1 );
			avatar.ik( "mixamorig_LeftFoot", cube.trackers['tracker_foot_left']['obj'], 1 );
		} else {
			console.log( "TRACKERS ARE NORMAL!" );
			avatar.ik( "mixamorig_RightFoot", cube.trackers['tracker_foot_left']['obj'], 1 );
			avatar.ik( "mixamorig_LeftFoot", cube.trackers['tracker_foot_right']['obj'], 1 );
		}
		
	}
	

}

function number_display( num ) {

	var str = "" + Math.floor( num * 100 ) / 100;
	var n = str.indexOf(".");
	if ( n == -1 ) {
		return str + ".00";
	} else if  ( n == str.length - 2 ) {
		return str + "0";
	}
	return str;

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	if ( fps_camera ) {
		fps_camera.aspect = window.innerWidth / window.innerHeight;
		fps_camera.updateProjectionMatrix();
	}
	renderer.setSize( window.innerWidth, window.innerHeight );
	
	init_shadow_viewer();
	
	//composer.setSize( window.innerWidth, window.innerHeight );
	//fps_controls.handleResize();

}

function animate() {

	requestAnimationFrame( animate );
	render();
	if ( stats_enabled ) {
		stats.update();
	}

}

function toggle_camera() {

	if ( !avatar.ready || !cube.ready ) {
		return;
	}

	if ( fps_camera_active ) {
		// tmngr.camera_tracking_enable(true);
		fps_camera_active = false;
	} else {
		// tmngr.camera_tracking_enable(false);
		fps_camera_active = true;
	}
	
	info_panel_pivot.visible = fps_camera_active;

}

function render() {

	var delta = clock.getDelta();
	
	if ( wait_to_load > 0 && cube_to_load != undefined ) {
		
		wait_to_load -= delta;
		if ( wait_to_load <= 0 ) {
			loadcube( cube_to_load );
		}
		
	}
	else if ( wait_to_hide_loading > 0 ) {
		
		wait_to_hide_loading -= delta;
		
		if ( wait_to_hide_loading <= 0 ) {
			
			init_timeline();
			document.getElementById("model_loading").className = 'closed';
			document.getElementById("timeline").className = 'opened';
			document.getElementById("ui_3d").className = 'opened';
			
			if ( sisyphus_cubes_current_name ) {
				
				var cinfo = undefined;
				
				for ( i in sisyphus_cubes_library ) {
					var c = sisyphus_cubes_library[i];
					if ( c.name == sisyphus_cubes_current_name ) {
						cinfo = c;
						break;
					}
				}
				
				if ( cinfo ) {
					cube.speed_frame = cube.maximum_frame * 1.0 / cinfo.duration;
					info_canvas_refresh( cinfo );
				}
				
				
				sisyphus_cubes_current_name = undefined;
				
			}
			
		}
		
	}
	
	if ( target_camera_position != undefined ) {
		
		camera_position_transition_current += delta;
		var pc = camera_position_transition_current / camera_position_transition_time;
		if ( pc > 1 ) {
			pc = 1;
		}
		
		var mu = ( 1 + Math.sin( -Math.PI * 0.5 + Math.PI * pc ) ) * 0.5;
		
		var newp = current_camera_position.clone();
		newp.lerp( target_camera_position, mu );
		camera.position.set( newp.x, newp.y, newp.z );
		
		var newt = current_camera_target.clone();
		newt.lerp( target_camera_target, mu );
		controls.target.set( newt.x, newt.y, newt.z );
		
		if ( pc == 1 ) {
			target_camera_position = undefined;
		}
		
	}
	
	controls.update();
	
	if ( avatar.ready === true && cube.ready === true ) {
		
		// info_panel_pivot.rotation.y = controls.getAzimuthalAngle();
		
		Interpolation.update( delta );

		head_tracker.update( delta );
		avatar.update( delta );
		cube.update( delta );
		
		// copying roation stored in cube on the avatar:
		
		
		if ( timeline_slider_enabled && cube.playing ) {
			inputRange = document.getElementById( 'timeline_range' );
			inputRange.value = cube.percent * 100;
			inputRange.dispatchEvent(new Event('change'));
		}
		
		working_time();
	
	}

	if ( fps_camera_active ) {
		renderer.render( scene, fps_camera );
		lightShadowMapViewer.render( renderer );
	} else {
		renderer.render( scene, camera );
	}
	
	if ( menu_close_timer > 0 ) {
		menu_close_timer -= delta;
		if ( menu_close_timer <= 0 && !menu_visible() ) {
			if ( cube === undefined || !cube.ready ) {
				gallery_item_click( parseInt( Math.random() * sisyphus_cubes_library.length - 1 ) );
			} else {
				goto_mainmenu();
			}
		}
	}
	
}

function working_time() {
	
	var content = "";
	//var millis = tmngr.morph_inter.current * timestamp_data['max'];
	//var secs = millis / 1000;
	var secs = cube.current_frame / cube.speed_frame;
	var h = Math.floor( secs / 3600 );
	var m = Math.floor( ( secs - h * 3600 ) / 60 );
	var s = Math.floor( secs - ( h * 3600 + m * 60 ) );
	if ( h < 10 ) content += "0";
	content += h + ":";
	if ( m < 10 ) content += "0";
	content += m + ":";
	if ( s < 10 ) content += "0";
	content += s;
	document.getElementById( 'duration' ).style.visibility = "visible";
	document.getElementById( 'duration' ).innerHTML = content;
	
}

function print( e ) {}

function debug( e ) {
	document.getElementById("debugger").innerHTML = e;
}

function debug_json( e ) {
	document.getElementById("debugger").innerHTML = JSON.stringify(e, null, 4);
}

// *********************************
// ******** CANVAS RELATED *********
// *********************************

function info_canvas_init() {
	
	info_canvas = document.createElement("canvas");
	info_canvas.id = "info_canvas";
	info_canvas.style.width = 512;
	info_canvas.style.height = 512;
	info_canvas.width = 512;
	info_canvas.height = 512;
	document.body.appendChild( info_canvas );
		
	info_canvas_context = info_canvas.getContext("2d");
	
	/*
	info_canvas_context.fillStyle = "white";
	info_canvas_context.fillRect( 10, 10, 10, 10 );
	info_canvas_context.fillRect( 482, 10, 10, 10 );
	info_canvas_context.fillRect( 482, 482, 10, 10 );
	info_canvas_context.fillRect( 10, 482, 10, 10 );
	*/
	
}

function info_canvas_dropshadow( txt, x, y, radius ) {
	
	var offx = 0;
	var offy = 0;
	
	for ( var i = 0; i < 7; ++i ) {
		
		if ( i < 6 ) {
			var a = Math.PI * 2 * i / 6.0;
			offx = Math.cos( a ) * radius;
			offy = Math.sin( a ) * radius;
			info_canvas_context.fillStyle = "black";
		} else {
			offx = 0;
			offy = 0;
			info_canvas_context.fillStyle = "white";
		}
		
		info_canvas_context.fillText( txt, x + offx, y + offy );
		
	}
	
}

function info_canvas_refresh( cinfo ) {
	
	if ( cinfo == undefined ) {
		return;
	}
	
	info_canvas_context.clearRect(0, 0, 512, 512 );
	
	var border = 2;
	
	var n = '' + cinfo.id;
	while( n.length < 3 ) {
		n = '0' + n;
	}
	
	var ytop = 120;
	
	info_canvas_context.textAlign="center";
	info_canvas_context.font = "90px space_monoregular";
	info_canvas_dropshadow( 'chair ' + n, 256, ytop, border ); 
	ytop += 70;
	info_canvas_context.font = "20px space_monoregular";
	info_canvas_dropshadow( 'produced on ' + cinfo.date + ' in', 256, ytop, border );
	ytop += 25;
	info_canvas_dropshadow( cinfo.duration_r, 256 , ytop, border );
	ytop += 25;
	info_canvas_dropshadow( cinfo.name, 256 , ytop, border );
	
	var t = new THREE.Texture( info_canvas );
	t.needsUpdate = true;
	info_panel_pivot.children[0].material.map = t;
	info_panel_pivot.children[0].material.needsUpdate = true;
	
	info_panel_pivot.visible = fps_camera_active;
	
}

function info_toggle() {
	
	/*
	info_visible = !info_visible;
	if ( info_visible ) {
		document.getElementById( 'display_info_icon' ).src = "textures/icon-no-question.png";
	} else {
		document.getElementById( 'display_info_icon' ).src = "textures/icon-question.png";
	}
	info_panel_pivot.visible = info_visible;
	*/
	
}

// *********************************
// ********** UI RELATED ***********
// *********************************

function cube_listing_load() {

	var xobj = new XMLHttpRequest();
	xobj.caller = this;
	xobj.overrideMimeType("application/json");
	xobj.open('GET', sisyphus_cubes_json, true);
	xobj.onreadystatechange = function () {
		if (xobj.readyState == 4 && xobj.status == "200") {
			on_cube_listing_loaded(xobj.responseText);
		} else if ( xobj.status != "200" ) {
			//on_cube_listing_loaded(xobj);
			// do nothing
		}
	}
	xobj.send(null);

};

function on_cube_listing_loaded( e ) {
	
	sisyphus_cubes_library = JSON.parse( e ).data;
	// console.log( data.data );
	//var minfo = document.getElementById("model_info");
	//minfo.innerHTML = '';
	for( var i in sisyphus_cubes_library ) {
		sisyphus_cubes_library[i].id = i;
		add_gallery_item( sisyphus_cubes_library[i] );
	}
	
	if ( request_chairid ) {
		gallery_item_click( request_chairid );
	}
	
}

function menu_visible() {
	
	return document.getElementById( 'ui_3d' ).style.visible == 'visible';

}

function menu_stop( el ) {
	
	el.className = '';
	el.style.visibility = 'hidden';

}

function menu_start( el ) {

	el.style.visibility = 'visible';
	el.style.opacity = '1';
	el.style.animation='menu_opening_anim 0.5s ease-out';
	var newone = el.cloneNode(true);
	el.parentNode.replaceChild(newone, el);

}

function goto_mainmenu() {
	
	menu_stop( document.getElementById( 'about_panel' ) );
	menu_stop( document.getElementById( 'inventory_panel' ) );
	menu_start( document.getElementById( 'ui_3d' ) );
	document.getElementById( 'container' ).className = '';

	document.getElementById("timeline").className = 'opened';
	
	menu_close_timer = -1;
	
}

function goto_about() {
	
	menu_stop( document.getElementById( 'ui_3d' ) );
	menu_stop( document.getElementById( 'inventory_panel' ) );
	menu_start( document.getElementById( 'about_panel' ) );
	document.getElementById( 'container' ).className = 'blurry';
	
	if (typeof MENU_TIMEOUT !== 'undefined') {
		menu_close_timer = MENU_TIMEOUT;
	}
	
}

function goto_inventory() {

	//gallery_request( true );
	menu_stop( document.getElementById( 'ui_3d' ) );
	menu_stop( document.getElementById( 'about_panel' ) );
	menu_start( document.getElementById( 'inventory_panel' ) );
	document.getElementById( 'container' ).className = 'blurry';
	
	document.getElementById("inventory_panel").className += '';
	document.getElementById("inventory_panel").scrollTop = inventory_scrolltop;
	
	document.getElementById("timeline").className = 'closed';
	
	if (typeof MENU_TIMEOUT !== 'undefined') {
		menu_close_timer = MENU_TIMEOUT;
	}

}

function init_timeline() {
	document.getElementById( 'timeline' ).style.visibility = 'visible';
}

function hide_timeline() {
	document.getElementById( 'timeline' ).style.visibility = 'hidden';
}

function shorten_duration( txt ) {
	return txt.replace( ' day,', 'd' ).replace( ' hours,', 'hrs' ).replace( ' minutes,', 'min' ).replace( ' seconds,', 'sec' )
}

function gallery_item_out() {
	document.getElementById( 'inventory_info' ).className = '';
	console.log( 'gallery_item_out' );
}

function gallery_item_hover( id ) {
	
	var text = '<div>';
	text += '<span>chair ';
	var n = '' + id;
	while ( n.length < 3 ) {
		n = '0' + n;
	}
	text += n + ",</span> <span>produced on ";
	var c = sisyphus_cubes_library[id];
	text += c.date + ' in ' + shorten_duration( c.duration_r );
	text += '</span>';
	text += '</div>';
	document.getElementById( 'inventory_info' ).innerHTML = text;
	var cn = document.getElementById( 'inventory_info' ).className;
	if ( cn != 'opened' ) {
		document.getElementById( 'inventory_info' ).className = 'opened';
	}
	
}

function gallery_item_click( id ) {
	
	var cube = sisyphus_cubes_library[id];
	
	if ( !cube ) {
		return;
	}
	
	if ( sisyphus_cubes_current_button != undefined ) {
		document.getElementById( sisyphus_cubes_current_button ).className = 'seen';
	}
	sisyphus_cubes_current_button = "button_" + cube.name;
	
	document.getElementById( sisyphus_cubes_current_button ).className = 'seen sel';
	
	sisyphus_cubes_current_name = cube.name;
	
	inventory_scrolltop = document.getElementById("inventory_panel").scrollTop;
	
	goto_mainmenu();
	
	document.getElementById("loading_img").src = cube.icon;
	var n = '' + id;
	while ( n.length < 3 ) {
		n = '0' + n;
	}
	document.getElementById("loading_name").innerHTML = "loading chair " + n;
	document.getElementById("model_loading").className = 'open';
	document.getElementById("ui_3d").className = 'closed';
	document.getElementById( 'inventory_info' ).className = '';
	document.getElementById( 'model_info' ).innerHTML = 
		'<span class="info_name">chair ' + n + '</span>, ' + 
		'<span class="info_date">produced on ' + cube.date + ' in ' + shorten_duration( cube.duration_r ) + '</span>';
	
	info_panel_pivot.visible = false;
	
	cube_to_load = cube;
	wait_to_load = 1.05;
	
	camera_position_transition_current = 0;
	current_camera_position = new THREE.Vector3( camera.position.x, camera.position.y, camera.position.z );
	target_camera_position = default_camera_position;
	current_camera_target = controls.target.clone();
	
}

function add_gallery_item( item ) {
	
	var gelm = document.getElementById("inventory_list");
	var cnum = document.getElementsByClassName("chair").length;
		
	var subdiv;
	var a;
	var im;
	var div = document.createElement("div");
		div.className = 'chair';
		div.alt = item.name;
	gelm.appendChild( div );
	
	var thumb = document.createElement("div");
	thumb.className = "thumb";
	
	thumb.innerHTML += '<button id="button_' + item.name + '" onmouseover="gallery_item_hover(' + item.id + ')" onclick="gallery_item_click(' + "'" + item.id + "'" + ')"/>';
	div.appendChild( thumb );
	var img = document.createElement("img");
		img.src = item.icon;
	document.getElementById( 'button_' + item.name ).appendChild( img );
	
}

// *********************************
// *********** SLIDER **************
// *********************************

// source: https://www.cssscript.com/responsive-touch-enabled-range-slider-in-vanilla-javascript-rangeslider/
try {
	(function () {

		var selector = '[data-rangeSlider]',
			elements = document.querySelectorAll(selector);
		
		// Basic rangeSlider initialization
		rangeSlider.create(elements, {
			onInit: function () {
			},
			onSlideStart: function (value, percent,  position) {
				if ( cube.ready ) {
					cube.playing = false;
				}
			},
			onSlide: function (value, percent, position) {
				if ( cube.ready ) {
					cube.playing = false;
					cube.current_frame = percent * cube.total_frames;
				}
			},
			onSlideEnd: function (value, percent,  position) {
				if ( cube.ready ) {
					cube.playing = true;
				}
			}
		});

		timeline_slider_enabled = true;
	
	})();
}
catch (e) {
	window.alert( "timeline slider is fucked up!" );
}

// *********************************
// *********** BOOTING *************
// *********************************

function autoclose_splash() {
	if ( close_splash_request ) {
		close_splash();
	} else {
		splash_finished = true;
	}
}

function close_splash() {
	
	var url = new URL( window.location.href );
	var chairid = url.searchParams.get("chair");
	
	document.getElementById( 'splash' ).outerHTML = '';
	
	if ( chairid ) {
		
		gallery_item_click( chairid );
		
	} else {
	
		goto_inventory();
		document.getElementById("inventory_panel").className += 'no_icon';
		document.getElementById( 'inventory_info' ).innerHTML = '<div><span>pick a chair to see the production process</span></div>';
		document.getElementById( 'inventory_info' ).className = 'opened';
	}
	
}

// checking the url
var url = new URL( window.location.href );
var url_skipspl = url.searchParams.get("skip");
request_chairid = url.searchParams.get("chair");

if ( url_skipspl || request_chairid ) {
	
	document.getElementById( 'splash' ).outerHTML = '';
	
} else {
	
	setTimeout( function(){ autoclose_splash(); }, 6500 );
	
}

// initialising rest

init_threejs();
document.getElementById( 'ui_3d' ).style.visibility = 'visible';
window.addEventListener( 'resize', onWindowResize, false );
animate();
