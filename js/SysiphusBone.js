/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

function SysiphusBone() {

	this.bone = undefined; // Threejs Bone
	this.parent = undefined; // SysiphusBone

	// rendered after apply, can not be undefined
	this.world_matrix = new THREE.Matrix4();
	this.world_position = new THREE.Vector3();
	this.world_quaternion = new THREE.Quaternion();
	this.world_scale = new THREE.Vector3();

	// backup animation
	this.backup_p = undefined; // Vector3
	this.backup_q = undefined; // Quaternion
	this.backup_s = undefined; // Vector3
	this.backup_wp = undefined; // Vector3
	this.backup_wq = undefined; // Quaternion
	this.backup_ws = undefined; // Vector3
	this.backup_wm = undefined; // Matrix4
	this.backup_apply = false;

	// tracking feature
	this.track_to = undefined;
	this.track_to_side = new THREE.Vector3(1,0,0);
	this.track_to_front = new THREE.Vector3(0,0,1);
	this.track_to_up = new THREE.Vector3(0,-1,0);
	this.track_to_weight = 1;

	// glue to feature
	this.glue_to = undefined;
	this.glue_to_weight = 1;

	// deformation
	this.deform = undefined;
	this.deform_weight = 1;

	// IK feature
	this.ik = undefined;
	this.ik_side = new THREE.Vector3( 1, 0, 0 );
	this.ik_front = new THREE.Vector3( 0, 1, 0 );
	this.ik_up = new THREE.Vector3( 0, 0, 1 );
	this.ik_weight = 1;
	this.ik_depth = 0;
	
	// rotation copy feature
	this.copy_quaternion = undefined;
	this.copy_quaternion_weight = 1;
	
	// position copy feature
	this.copy_position = undefined;
	this.copy_position_weight = 1;

	this.render_world_mat = function( mat, use_backup ) {
		if ( this.parent !== undefined ) {
			if ( use_backup ) {
				mat.copy(
					this.parent.backup_wm.clone().multiply( this.bone.matrix )
				);
			} else {
				mat.copy(
					this.parent.world_matrix.clone().multiply( this.bone.matrix )
				);
			}
		} else if ( this.bone.parent !== undefined ) {
			mat.copy(
				this.bone.parent.matrixWorld.clone().multiply(
					this.bone.matrix
				)
			);
		} else {
			mat.copy( this.bone.matrixWorld );
		}
	};

	this.render_world = function() {

		this.bone.updateMatrix();
		this.render_world_mat( this.world_matrix );
		this.world_matrix.decompose(
			this.world_position,
			this.world_quaternion,
			this.world_scale );

	};

	this.backup = function() {

		if ( this.backup_p === undefined ) {
			this.backup_p = new THREE.Vector3();
			this.backup_q = new THREE.Quaternion();
			this.backup_s = new THREE.Vector3();
			this.backup_wp = new THREE.Vector3();
			this.backup_wq = new THREE.Quaternion();
			this.backup_ws = new THREE.Vector3();
			this.backup_wm = new THREE.Matrix4();
		}

		this.backup_p.copy( this.bone.position );
		this.backup_q.copy( this.bone.quaternion );
		this.backup_s.copy( this.bone.scale );

		this.render_world_mat( this.backup_wm, true );
		this.backup_wm.decompose(
			this.backup_wp,
			this.backup_wq,
			this.backup_ws
		);

		this.render_world();

	};

	this.restore = function() {

		if ( this.backup_apply && this.backup_p !== undefined ) {
			this.bone.position.copy( this.backup_p );
			this.bone.quaternion.copy( this.backup_q );
			this.bone.scale.copy( this.backup_s );
			this.render_world();
			this.backup_apply = false;
		}

	};

	this.apply = function() {

		if ( this.bone === undefined ) {
			return;
		}

		this.backup();

		if ( this.glue_to !== undefined ) {
			this.apply_glue();
			this.render_world();
			this.backup_apply = true;
		}

		if ( this.deform !== undefined ) {
			this.apply_deform();
			this.render_world();
			this.backup_apply = true;
		}

		if ( this.track_to !== undefined ) {
			this.apply_track();
			this.render_world();
			this.backup_apply = true;
		}

		if ( this.ik !== undefined ) {
			this.apply_ik();
			this.backup_apply = true;
		}

		if ( this.copy_quaternion !== undefined ) {
			this.apply_copy_quaternion();
			this.render_world();
			this.backup_apply = true;
		}

		if ( this.copy_position !== undefined ) {
			this.apply_copy_position();
			this.render_world();
			this.backup_apply = true;
		}

	};
	
	this.apply_copy_position = function() {
		
		var tgrt =  this.world_position.clone();
		tgrt.lerp( this.copy_position, this.copy_position_weight );
		
		if ( this.bone.parent === undefined ) {

			this.bone.position.copy( tgrt );

		} else {

			var qu = new THREE.Quaternion(0,0,0,1);
			var sc = new THREE.Vector3(1,1,1);
			var mat = new THREE.Matrix4().compose ( tgrt, qu, sc );
			parent_mat = new THREE.Matrix4().getInverse( this.parent.world_matrix );
			parent_mat.multiply(mat);
			mat = parent_mat;
			mat.decompose ( tgrt, qu, sc );
			this.bone.position.copy( tgrt );

		}
		
	};
	
	this.apply_copy_quaternion = function() {
		
		var tgrt =  this.world_quaternion.clone();
		tgrt.slerp( this.copy_quaternion, this.copy_quaternion_weight );
		
		if ( this.bone.parent === undefined ) {

			this.bone.quaternion.copy( tgrt );

		} else {

			var po = new THREE.Vector3(0,0,0);
			var sc = new THREE.Vector3(1,1,1);
			var mat = new THREE.Matrix4().compose ( po, tgrt, sc );
			parent_mat = new THREE.Matrix4().getInverse( this.parent.world_matrix );
			parent_mat.multiply( mat );
			mat = parent_mat;
			mat.decompose ( po, tgrt, sc );
			this.bone.quaternion.copy( tgrt );

		}
		
	};

	this.apply_glue = function() {

		if ( this.glue_to_weight <= 0 ) {
			return;
		}
		var target_pos = new THREE.Vector3();
		var target_quat = new THREE.Quaternion();
		this.glue_to.getWorldPosition( target_pos );
		this.glue_to.getWorldQuaternion( target_quat );

		target_pos.lerp( this.world_position, ( 1 - this.glue_to_weight ) );
		target_quat.slerp( this.world_quaternion, ( 1 - this.glue_to_weight ) );

		if ( this.bone.parent === undefined ) {

			this.bone.position.copy( target_pos );
			this.bone.quaternion.copy( target_quat );

		} else {

			var sc = new THREE.Vector3(1,1,1);
			var mat = new THREE.Matrix4().compose ( target_pos, target_quat, sc );
			parent_mat = new THREE.Matrix4().getInverse( this.parent.world_matrix );
			parent_mat.multiply(mat);
			mat = parent_mat;
			mat.decompose ( target_pos, target_quat, sc );

			this.bone.position.copy( target_pos );
			this.bone.quaternion.copy( target_quat );

		}

	};

	this.apply_ik = function() {

		// getting root of the ik at depth 2,
		// this.ik_depth is not yet used
		if (
			this.ik_weight <= 0 ||
			this.bone.parent === undefined ||
			this.bone.parent.parent === undefined
			) {
			return;
		}

		var eulers;
		var b_origin = this.parent.parent;
		var b_parent = this.parent;

		b_origin.backup_apply = true;
		b_parent.backup_apply = true;

		var target_pos = new THREE.Vector3();
		this.ik.getWorldPosition( target_pos );

		var target_quaternion = new THREE.Quaternion();
		this.ik.getWorldQuaternion( target_quaternion );

		var target_vector = target_pos.clone().add(
				b_origin.world_position.clone().multiplyScalar(-1) );

		var parent_vector = b_parent.world_position.clone().add(
				b_origin.world_position.clone().multiplyScalar(-1) );

		var bone_vector = this.world_position.clone().add(
				b_parent.world_position.clone().multiplyScalar(-1) );

		var distance_target = target_vector.length();
		var total_bones = parent_vector.length() + bone_vector.length();

		if ( distance_target >= total_bones ) {

			var direct;
			var corrected;
			var o_side;
			var o_front;
			var o_up;

			direct = new THREE.Quaternion().setFromUnitVectors(
				this.ik_front,
				target_vector.clone().normalize() );

			o_up = new THREE.Vector3().copy(this.ik_up).applyQuaternion(
				b_origin.world_quaternion );

			var proj_side = new THREE.Vector3().copy(
				this.ik_side
			).applyQuaternion( direct );

			var proj_front = new THREE.Vector3().copy(
				this.ik_front
			).applyQuaternion( direct );

			var proj_up = new THREE.Vector3().copy(
				this.ik_up
			).applyQuaternion( direct );

			var real_up = new THREE.Vector3();
			real_up.add( new THREE.Vector3().copy(
				proj_up
			).multiplyScalar( proj_up.dot( o_up ) ) );

			real_up.add( new THREE.Vector3().copy(
				proj_side
			).multiplyScalar( proj_side.dot( o_up ) ) );

			real_up.normalize();

			var real_side = new THREE.Vector3().copy( proj_front );
			real_side.cross( real_up ).normalize();

			corrected = new THREE.Quaternion().setFromRotationMatrix(
				new THREE.Matrix4().identity().makeBasis(
					real_side,
					proj_front,
					real_up
				)
			);

			if ( b_origin.bone.parent !== undefined ) {
				var q = new THREE.Quaternion();
				b_origin.bone.parent.getWorldQuaternion( q );
				q = q.conjugate().multiply( corrected );
			}

			// eulers = new THREE.Euler().setFromQuaternion( q );
			b_origin.bone.quaternion.slerp( q, this.ik_weight * this.ik_weight );
			b_origin.render_world();

			q = new THREE.Quaternion().copy(
				b_origin.world_quaternion
			).conjugate().multiply( corrected );

			eulers = new THREE.Euler().setFromQuaternion( q );
			b_parent.bone.quaternion.slerp( q, this.ik_weight );
			b_parent.render_world();

		} else {

			// preparing spheres
			var sphereA_pos = b_origin.world_position;
			var sphereB_pos = target_pos;
			var sphereA_radius = parent_vector.length();
			var sphereB_radius = bone_vector.length();

			var spheres_center_dir = new THREE.Vector3(
					sphereB_pos.x - sphereA_pos.x,
					sphereB_pos.y - sphereA_pos.y,
					sphereB_pos.z - sphereA_pos.z );

			var spheres_center_dist = spheres_center_dir.length();
			spheres_center_dir.normalize();

			var a =
			    ( 1.0 / ( 2.0 * spheres_center_dist ) ) *
			    Math.sqrt(
			      (
							4 * Math.pow(spheres_center_dist,2) *
							Math.pow(sphereA_radius,2) ) -
				      Math.pow( Math.pow(spheres_center_dist,2) -
							Math.pow(sphereB_radius,2) +
							Math.pow(sphereA_radius,2), 2
						)
			    );

			// rendering parent target

			var o_up = new THREE.Vector3().copy(
				this.ik_up
			).applyQuaternion( b_origin.world_quaternion );

			var spheres_center_quat = new THREE.Quaternion().setFromUnitVectors(
				this.ik_up,
				spheres_center_dir.clone().normalize() );

			// projections of side and front on circle plane using up as normal
			var proj_side = new THREE.Vector3().copy(
				this.ik_side
			).applyQuaternion( spheres_center_quat );

			var proj_front = new THREE.Vector3().copy(
				this.ik_front
			).applyQuaternion( spheres_center_quat );

			var proj_up = new THREE.Vector3().copy(
				this.ik_up
			).applyQuaternion( spheres_center_quat );

			var real_front = new THREE.Vector3();
			real_front.add( new THREE.Vector3().copy(
				proj_front
			).multiplyScalar( proj_front.dot( o_up ) ) );

			real_front.add( new THREE.Vector3().copy(
				proj_side
			).multiplyScalar( proj_side.dot( o_up ) ) );

			real_front.normalize();

			var real_side = new THREE.Vector3().copy( real_front );
			real_side.cross( proj_up ).normalize();

			spheres_center_quat = new THREE.Quaternion().setFromRotationMatrix(
				new THREE.Matrix4().identity().makeBasis(
					real_side,
					real_front,
					proj_up
				)
			);

			// getting a direction vector
			var front = new THREE.Vector3().copy(
				this.ik_front
			).applyQuaternion( spheres_center_quat );

			front.multiplyScalar( a );

			var length =
				Math.sqrt( Math.pow( sphereA_radius,2 ) -
				Math.pow( a,2 ) );

			spheres_center_dir.multiplyScalar( length );

			// relative position of the parent in origin space
			var mid_pos = front.clone().add( spheres_center_dir );
			var mid_pos_norm = mid_pos.clone().normalize();

			// we now have the new point!
			// we need to compute the rotatio manually, as nothing else gives
			// good results

			// starting by rendering the vector mid_pos > target
			var end_pos = new THREE.Vector3(
					sphereB_pos.x - ( sphereA_pos.x + mid_pos.x ),
					sphereB_pos.y - ( sphereA_pos.y + mid_pos.y ),
					sphereB_pos.z - ( sphereA_pos.z + mid_pos.z ) );
			var end_pos_norm = end_pos.clone().normalize();

			var norm_pos;
			var tangent_pos;
			var rotation_mat;

			// we can now compute the normal, representing the x rotation
			// component of the rotation
			norm_pos = end_pos_norm.clone().cross( mid_pos_norm ).normalize();

			// we also need the Z rotation; for the origin,
			// it is computed with mid_pos
			tangent_pos = norm_pos.clone().cross( mid_pos_norm ).normalize();

			// now we can compose the rotation matrix
			rotation_mat = new THREE.Matrix4().identity().makeBasis(
				norm_pos,
				mid_pos_norm,
				tangent_pos
			);

			// conversion of the matrix to local space
			if ( b_origin.bone.parent !== undefined ) {
				var m = new THREE.Matrix4().getInverse(
					b_origin.bone.parent.matrixWorld
				);
				m.multiply( rotation_mat );
				rotation_mat = m;
			}

			// and applying it
			var q = new THREE.Quaternion().setFromRotationMatrix( rotation_mat );
			b_origin.bone.quaternion.slerp( q, this.ik_weight * this.ik_weight );

			// updating matrixWorld of the origin
			b_origin.render_world();

			// and starting the same process on the second segment
			tangent_pos = norm_pos.clone().cross( end_pos_norm ).normalize();
			rotation_mat = new THREE.Matrix4().identity().makeBasis(
				norm_pos,
				end_pos_norm,
				tangent_pos
			);
			var m = new THREE.Matrix4().getInverse( b_origin.world_matrix );
			m.multiply( rotation_mat );
			rotation_mat = m;
			var q = new THREE.Quaternion().setFromRotationMatrix( rotation_mat );
			b_parent.bone.quaternion.slerp( q, this.ik_weight );
			b_parent.render_world();

		}

		var q = new THREE.Quaternion().copy(
			b_parent.world_quaternion
		).conjugate().multiply( target_quaternion );

		//this.bone.quaternion.slerp( q, this.ik_weight );

		return;

	};

	this.apply_deform = function() {

		this.bone.scale.lerp( this.deform, this.deform_weight );

	};

	this.apply_track = function() {

		var tracked_wpos = new THREE.Vector3();
		this.track_to.getWorldPosition( tracked_wpos );

		// expressing the tracked position in the bone space
		if ( this.parent !== undefined ) {
			var tmat = new THREE.Matrix4().identity().makeTranslation(
				tracked_wpos.x,
				tracked_wpos.y,
				tracked_wpos.z );
			var parent_mati = new THREE.Matrix4().getInverse(
				this.parent.world_matrix );
			var m = new THREE.Matrix4().multiplyMatrices(
				parent_mati,
				tmat );
			tracked_wpos = new THREE.Vector3().applyMatrix4( m );
		}

		// bone front
		var side = this.track_to_side.clone();
		var front = this.track_to_front.clone();
		var up = this.track_to_up.clone();

		front.copy( tracked_wpos );
		front.normalize();
		up.applyQuaternion( this.bone.quaternion );
		up.normalize();
		side = front.clone();
		side.cross( up );
		side.normalize();
		up = front.clone()
		up.cross( side );
		up.normalize();

		var q = new THREE.Quaternion().setFromRotationMatrix(
				new THREE.Matrix4().identity().makeBasis( side, up, front ) );
		this.bone.quaternion.slerp( q, this.track_to_weight );

	};

	this.update_matrices = function() {

		this.bone.updateMatrix();
		this.world_matrix.compose(
			this.world_position,
			this.world_quaternion,
			this.world_scale );

	};

};
