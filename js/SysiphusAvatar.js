/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

function SysiphusAvatarConfig() {

	this.mesh = undefined;
	this.skeleton_root = undefined;

	this.path_model = undefined;
	this.path_animations = undefined;
	this.name_animations = undefined;
	this.path_deforms = undefined;
	this.name_default_animation = undefined;

	// animation configuration
	this.weight_init = 0;
	this.weight_speed = 1.3;
	this.weight_method = Interpolation.sin;

	this.ready_callbak = undefined;
	this.debug_skeleton = true;

	this.diffuse_map = undefined;
	this.specular_map = undefined;
	this.normal_map = undefined;

	this.clone = function() {
		var out = new SysiphusAvatarConfig();
		out.mesh = this.mesh;
		out.skeleton_root = this.skeleton_root;
		out.path_model = this.path_model;
		out.path_animations = this.path_animations.slice();
		out.name_animations = this.name_animations.slice();
		out.path_deforms = this.path_deforms.slice();
		out.name_default_animation = this.name_default_animation;
		out.weight_init = this.weight_init;
		out.weight_speed = this.weight_speed;
		out.weight_method = this.weight_method;
		out.ready_callbak = this.ready_callbak;
		out.debug_skeleton = this.debug_skeleton;
		out.diffuse_map = this.diffuse_map;
		out.specular_map = this.specular_map;
		out.normal_map = this.normal_map;
		return out;
	};

};

function SysiphusAvatar() {

	this.obj3d = undefined;
	this.obj3d_skeletonhelper = undefined;
	this.obj3d_mesh = undefined;

	this.config = undefined;

	this.scene = undefined;
	this.animations = undefined;
	this.mixer = undefined;

	// bones management
	this.bones = undefined; // map of SysiphusBone
	this.bones_array = undefined; // list of SysiphusBone
	this.deforms = undefined; // map of BoneScalePresets

	this.ready = false;

	// animation request handling
	this.current_anim = undefined; // animation object
	this.request_anim = undefined; // name of the anim
	this.on_loop_anim = undefined; // anim to call at the end of the request

	// scaling handling
	this.deform_lerp_alpha = 0;
	this.deform_default = undefined;
	this.deform_target = undefined;
	this.deform_current = undefined;

	// utilities

	this.move = function( x, y, z ) {

		if ( this.ready === false ) return;
		this.obj3d.position.set(x, y, z);

	};

	this.ik = function( name, obj3d, weight ) {

		if ( this.ready === false ) return;
		this.bones[name].ik = obj3d;
		this.bones[name].ik_weight = weight;

	};

	this.track_to = function( name, obj3d, weight ) {

		if ( this.ready === false ) return;
		this.bones[name].track_to = obj3d;
		this.bones[name].track_to_weight = weight;

	};

	this.glue_to = function( name, obj3d, weight ) {

		if ( this.ready === false ) return;
		this.bones[name].glue_to = obj3d;
		this.bones[name].glue_to_weight = weight;

	};

	this.copy_quat = function( name, quat, weight ) {

		if ( this.ready === false ) return;
		this.bones[name].copy_quaternion = quat;
		this.bones[name].copy_quaternion_weight = weight;

	};

	this.copy_pos = function( name, pos, weight ) {

		if ( this.ready === false ) return;
		this.bones[name].copy_position = pos;
		this.bones[name].copy_position_weight = weight;

	};

	this.goto_deform = function( name ) {

		if ( this.ready === false ) return;
		if ( name == this.deform_target.name ) {
			// console.log( "preset " + name + " is already loaded" );
			return;
		}
		this.deform_target = this.deforms[name];
		this.deform_lerp_alpha = 0;

	};

	this.visibility = function( display ) {

		if ( this.ready === false ) return;
		this.obj3d.visible = display;
		this.obj3d_skeletonhelper.visible = display;

	};

	// loading and management

	this.configure = function( config ) {

		this.config = config.clone();
		this.config.name_animations.unshift( "__default__" );

	};

	this.load = function( scene ) {

		if ( this.config === undefined ) {
			console.log( "You have to configure this SysiphusAvatar before loading it!" );
			console.log( "Create a SysiphusAvatarConfig and pass it to 'configure()' " );
			return;
		}

		this.scene = scene;

		// print( "SysiphusAvatar start loading,<br/>loading display: " + this.config.path_model );
		var loader = new THREE.ColladaLoader();
		loader.caller = this;
		loader.path_loaded = this.config.path_model;

		loader.load( loader.path_loaded, function ( collada ) {

			var sa  = loader.caller;
			var conf = sa.config;

			sa.animations = collada.animations;
			sa.obj3d = collada.scene;
			sa.scene.add( sa.obj3d );

			sa.obj3d.traverse( function ( child ) {

				if ( child.name == conf.skeleton_root ) {

					// console.log( "found #" + conf.skeleton_root + " in " + loader.path_loaded + " !!!" );

					sa.bones = {};
					sa.bones_array = [];
					sa.load_bone( child );

					if (conf.debug_skeleton) {
						sa.obj3d_skeletonhelper = new THREE.SkeletonHelper( child );
						sa.obj3d_skeletonhelper.visible = true;
						sa.scene.add( sa.obj3d_skeletonhelper );
					}

				} else if ( child.name == conf.mesh ) {
					
					child.castShadow = SHADOW_ENABLED;
					child.receiveShadow = SHADOW_ENABLED;
					sa.obj3d_mesh = child;
					//sa.obj3d_mesh.visible = false;

				} else if ( sa.bones !== undefined && sa.obj3d_mesh !== undefined ) {

					return;

				}

			} );

			// all is done for display model,
			// now we can start loading animations
			// animation list loading
			sa.load_next_animation();

		} );

	};

	this.load_bone = function( obj, parent ) {

		var sb = new SysiphusBone();
		sb.bone = obj;
		sb.parent = parent;
		this.bones_array.push( sb );
		this.bones[ obj.name ] = sb;
		for( var i = 0; i < obj.children.length; ++i ) {
			this.load_bone( obj.children[i], sb );
		}

	};

	this.load_next_animation = function() {

		if ( this.config.path_animations.length > 0 ) {

			var al = this.config.path_animations.shift();

			var loader = new THREE.ColladaLoader();
			loader.caller = this;
			loader.path_loaded = al;

			loader.load( al, function ( collada ) {

				var sa  = loader.caller;
				var conf = sa.config;

				console.log( "loader.path_loaded:",loader.path_loaded );

				var alist = collada.animations;
				for ( var i = 0; i < sa.animations[0].tracks.length; ++i ) {
					alist[ 0 ].tracks[i].name = sa.animations[0].tracks[i].name;
				}
				sa.animations.push( alist[ 0 ] );

				// print( "SisyphusAvatar: anim '" + loader.path_loaded + "' loaded" );

				sa.load_next_animation();

			} );

			return;

		}

		print( "SisyphusAvatar: all animations loaded" );

		this.on_loading_animation_complete();

	};

	this.on_loading_animation_complete = function() {

		// all is done for models and animations,
		// now we can start loading scale presets
		this.load_deforms();

	};

	this.load_deforms = function() {

		this.deforms = {};

		this.deform_default = new BoneScalePreset();
		this.deform_default.init( this.bones );
		this.deforms[ this.deform_default.name ] = this.deform_default;

		this.deform_target = this.deform_default;
		this.deform_current = this.deform_default;

		this.load_next_deform();

	};

	this.load_next_deform = function() {

		if ( this.config.path_deforms.length == 0 ) {

			// print( "SisyphusAvatar: all deforms loaded" );
			this.on_loading_complete();
			return;

		}
		var path = this.config.path_deforms.shift();

		var xobj = new XMLHttpRequest();
		xobj.caller = this;
		xobj.path_loaded = path;
		xobj.overrideMimeType("application/json");
		xobj.open('GET', path, true);
		xobj.onreadystatechange = function () {
			if (xobj.readyState == 4 && xobj.status == "200") {
				// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
				print( "SisyphusAvatar: deform '" + this.path_loaded + "' loaded" );
				this.caller.json_load_success(xobj.responseText);
			} else if ( xobj.status != "200" ) {
				this.caller.json_load_failed(xobj);
			}
		}
		xobj.send(null);

	};

	this.json_load_success = function( e ) {

		try{
			var data = JSON.parse( e );
			var bsp = new BoneScalePreset();
			bsp.init( this.bones, data );
			this.deforms[ bsp.name ] = bsp;
		} catch( exception ) {
			console.log( "parsing of " + e + " failed, \n" + exception );
		}
		this.load_next_deform();

	};

	this.json_load_failed = function( e ) {

		console.log( "loading of " + e + " failed" );
		this.load_next_deform();

	};

	this.on_loading_complete = function() {

		// renaming animations
		for ( var i = 0; i < this.animations.length; ++i ) {
			if ( i < this.config.name_animations.length ) {
				this.animations[i].name = this.config.name_animations[i];
			} else {
				this.animations[i].name = "anim_" + i;
			}
			this.animations[i].loop  = THREE.LoopOnce;
			this.animations[i].weight = Interpolation.new_interpolation(
				this.animations[i].name,
				this.config.weight_init,
				this.config.weight_speed,
				this.config.weight_method
			);
		}
		// console.log( "all animations are loaded" );

		// this.mixer = new THREE.AnimationMixer( this.avatar_process );
		this.mixer = new THREE.AnimationMixer( this.obj3d );
		this.mixer.caller = this;
		this.mixer.addEventListener( 'loop', function( e ) {
			this.caller.on_loop_event(e);
		} );

		for ( var i = 0; i < this.animations.length; ++i ) {
			var track = this.mixer.clipAction( this.animations[i] );
			track.setEffectiveWeight( 0 );
			track.enabled = true;
			track.just_looped = false;
		}
		// enabing the first animation
		this.request_anim = "" + this.config.name_default_animation;
		this.on_loop_anim = this.request_anim;

		if (
			this.config.diffuse_map !== undefined ||
			this.config.normal_map !== undefined
		) {

			var textureLoader = new THREE.TextureLoader();
			var mat = new THREE.MeshPhongMaterial( {
				morphTargets: this.obj3d_mesh.material.morphTargets,
				morphNormals: this.obj3d_mesh.material.morphNormals,
				skinning: this.obj3d_mesh.material.skinning,
				color: 0xffffff,
				specular: 0xdddddd,
				shininess: 5
			} );
			if ( this.config.diffuse_map !== undefined ) {
				mat.map = textureLoader.load( this.config.diffuse_map );
			}
			if ( this.config.diffuse_map !== undefined ) {
				mat.specularMap = textureLoader.load( this.config.specular_map );
			}
			if ( this.config.normal_map !== undefined ) {
				mat.normalMap = textureLoader.load( this.config.normal_map );
				mat.normalScale = new THREE.Vector2( 3,3 );
			}
			this.obj3d_mesh.material = mat;
			this.obj3d_mesh.material.needsUpdate = true;

		}

		this.ready = true;

		if ( this.config.ready_callbak !== undefined ) {
			this.config.ready_callbak();
		}

		// console.log( this );

	};

	this.on_loop_event = function( e ) {

		if (
			e.action._clip.name == this.current_anim.name &&
			this.on_loop_anim != this.current_anim.name
		) {
			this.set_animation_name( this.on_loop_anim );
		}

		for ( var i = 0; i < this.animations.length; ++i ) {
			var a = this.animations[i];
			if ( e.action._clip.name == a.name ) {
				this.mixer.clipAction( a ).just_looped = true;
				return;
			}
		}

	};

	this.get_animation_name = function() {
		return this.current_anim.name;
	};

	this.set_animation_name = function( name, on_loop ) {

		if (
			name == this.current_anim.name &&
			(
				on_loop === undefined ||
				on_loop == this.on_loop_anim
			)
		) {
			return;
		}

		this.request_anim = name;
		if ( on_loop === undefined ) {
			on_loop = name;
		}
		this.on_loop_anim = on_loop;

	};

	this.switch_animation = function() {

		if ( this.mixer === undefined || this.request_anim === undefined ) {
			return;
		}

		var prev_anim = this.current_anim;

		for ( var i = 0; i < this.animations.length; ++i ) {
			if ( this.animations[i].name == this.request_anim ) {
				this.animations[i].weight.target = 1;
				var track = this.mixer.clipAction( this.animations[i] );
				track.enabled = true;
				track.stopFading();
				track.play();
				this.current_anim = this.animations[i];
			} else {
				this.animations[i].weight.target = 0;
			}
		}

		if ( prev_anim == this.current_anim ) {
			// console.log( "SAME ANIM! " + this.current_anim.name );
			return;
		} else {
			// console.log( "Swapping to " + this.current_anim.name );
		}
		this.request_anim = undefined;

	};

	this.fade_animations = function( delta ) {

		if ( this.mixer !== undefined ) {

			this.switch_animation();

			if ( this.current_anim === undefined ) {
				console.log("no anim...");
				return;
			}

			for ( var i = 0; i < this.animations.length; ++i ) {

				var track = this.mixer.clipAction( this.animations[i] );
				track.just_looped = false;

				if ( !this.animations[i].weight.idle ) {
					var w = this.animations[i].weight.value;
					track.setEffectiveWeight( w );
					if ( w == 0 ) {
						track.stop();
					}
				}

			}

		}

	};

	this.apply_anim = function() {

		for( var b in this.bones_array ) {
			this.bones_array[b].apply();
		}

	};

	this.update_deforms = function( delta ) {

		var do_lerp = false;

		if ( this.deform_current != this.deform_target ) {
			this.deform_lerp_alpha += delta;
			do_lerp = true;
			if ( this.deform_lerp_alpha >= 1 ) {
				this.deform_lerp_alpha = 1;
			}
		}

		if ( do_lerp ) {

			for( var name in this.bones ) {
				this.bones[name].deform =
					this.deform_target.lerp(
						this.deform_current, name,
						Interpolation.sin( this.deform_lerp_alpha )
						);
			}

		}

		if ( this.deform_lerp_alpha >= 1 ) {
			this.deform_lerp_alpha = 0;
			this.deform_current = this.deform_target;
		}

	};

	this.restore = function() {
		for( var b in this.bones_array ) {
			this.bones_array[b].restore();
		}
	};

	this.update = function( delta ) {

		if ( this.ready === false ) return;

		this.restore();
		this.fade_animations( delta );

		this.mixer.update( delta );

		this.update_deforms( delta );
		this.apply_anim();

	};

};
