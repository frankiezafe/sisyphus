import bpy
import mathutils

'''
reads all quaternions of current pose and print in console as a dictionnary
dictionnary keys: bone name
dictionnary values: tuple with quaternion values
@param
	name of the armature
'''
def print_pose( arma_name ):
	
	print( "************** print_pose() start **************" )

	arma_obj = bpy.data.objects[ arma_name ]
	arma = arma_obj.pose
	
	current_pose = "{"

	bcount = 0
	for bone in arma.bones:

		if bcount > 0:
			current_pose += ", "

		current_pose += "\'" + bone.name + "\':("
		current_pose += str( bone.rotation_quaternion[0] ) + ", "
		current_pose += str( bone.rotation_quaternion[1] ) + ", "
		current_pose += str( bone.rotation_quaternion[2] ) + ", "
		current_pose += str( bone.rotation_quaternion[3] )
		current_pose += ")"

		bcount += 1

	current_pose += "}"

	print( current_pose )
	
	print( "************** print_pose() end **************" )

'''
loads a specific pose on an armature
@param
	name of the armature
	dictionnay: key = bone name, value = tuple with quaternion values
'''
def load_pose( arma_name, pose ):
	
	arma_obj = bpy.data.objects[ arma_name ]
	arma = arma_obj.pose
	
	bpy.context.scene.objects.active = arma_obj
	bpy.ops.object.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	bpy.ops.object.select_all(action='DESELECT')
	#bpy.ops.object.select_pattern(pattern=arma_name)
	bpy.ops.object.mode_set(mode='POSE')
	
	for bone in arma.bones:
		quat = mathutils.Quaternion( pose[ bone.name ] )
		#print( bone, quat )
		bone.rotation_quaternion = quat
	
	bpy.ops.object.mode_set(mode='OBJECT')
	bpy.ops.object.select_all(action='DESELECT')

def load_slerp_poses( arma_name, pose0, pose1, alpha ):
	
	arma_obj = bpy.data.objects[ arma_name ]
	arma = arma_obj.pose
	pose = {}
	
	for bone in arma.bones:
		quat0 = mathutils.Quaternion( pose0[ bone.name ] )
		quat1 = mathutils.Quaternion( pose1[ bone.name ] )
		q = quat0.slerp( quat1, alpha )
		pose[ bone.name ] = ( q[0], q[1], q[2], q[3] )
	
	load_pose( arma_name, pose )

def load_random_pose( arma_name ):
	
	cid0 = int( mathutils.noise.random() * ( len( pose_list ) - 1 ) )
	cid1 = int( mathutils.noise.random() * ( len( pose_list ) - 1 ) )
	alpha = mathutils.noise.random()
	
	print( "mixing pose", cid0, "with", cid1, ", alpha:", alpha )
	
	load_slerp_poses( arma_name, pose_list[ cid0 ], pose_list[ cid1 ], alpha )
	
pose_list = []
# initial
pose_list.append( {'root':(1.0, 0.0, 0.0, 0.0), 'hip_L':(1.0, 0.0, 0.0, 0.0), 'upperleg_L':(1.0, 0.0, 0.0, 0.0), 'leg_L':(1.0, 0.0, 0.0, 0.0), 'hip_R':(1.0, 0.0, 0.0, 0.0), 'upperleg_R':(1.0, 0.0, 0.0, 0.0), 'leg_R':(1.0, 0.0, 0.0, 0.0)} )
# tailleur
pose_list.append( {'root':(1.0, 0.0, 0.0, 0.0), 'hip_L':(1.0, 0.0, 0.0, 0.0), 'upperleg_L':(0.853783130645752, 0.366218239068985, -0.340086966753006, 0.14587566256523132), 'leg_L':(0.7087972164154053, -0.7054123878479004, -1.767170942912344e-07, -7.462822537718239e-08), 'hip_R':(1.0, 0.0, 0.0, 0.0), 'upperleg_R':(0.8362533450126648, 0.39717304706573486, 0.34150615334510803, -0.16219620406627655), 'leg_R':(0.5366555452346802, -0.8438015580177307, 8.917520233353571e-08, 1.448607633847132e-07)} )
# wide open
pose_list.append( {'root':(1.0, 0.0, 0.0, 0.0), 'hip_L':(1.0, 0.0, 0.0, 0.0), 'upperleg_L':(0.8886080384254456, 0.12094464898109436, -0.028764626011252403, 0.441498339176178), 'leg_L':(0.970582127571106, -0.24077068269252777, 3.070562470952609e-08, 2.7107310174301347e-08), 'hip_R':(1.0, 0.0, 0.0, 0.0), 'upperleg_R':(0.9203392863273621, 0.14903244376182556, 0.08222314715385437, -0.3521423935890198), 'leg_R':(0.9756578803062439, -0.21929827332496643, -7.636468346561287e-09, 1.431837937104774e-08)} )
# accroupi
pose_list.append( {'root':(1.0, 0.0, 0.0, 0.0), 'hip_L':(1.0, 0.0, 0.0, 0.0), 'upperleg_L':(0.9175531268119812, -0.32363080978393555, 0.07683564722537994, 0.21784336864948273), 'leg_L':(0.3376120924949646, -0.9412853717803955, 2.551666682393261e-07, -4.7171346295726835e-07), 'hip_R':(1.0, 0.0, 0.0, 0.0), 'upperleg_R':(0.9188348054885864, -0.3350473642349243, 0.006266191601753235, -0.20844024419784546), 'leg_R':(0.30799275636672974, -0.9513887166976929, -1.5425885635522718e-07, 2.6720127266344207e-07)} )
# jambe gauche croisée ouverte
pose_list.append( {'root':(1.0, 0.0, 0.0, 0.0), 'hip_L':(1.0, 0.0, 0.0, 0.0), 'upperleg_L':(0.7443562746047974, 0.20229847729206085, -0.5997703075408936, 0.21280235052108765), 'leg_L':(0.5829612612724304, -0.8124999403953552, -2.003662729066491e-07, 2.2044488900974102e-08), 'hip_R':(1.0, 0.0, 0.0, 0.0), 'upperleg_R':(0.9687809348106384, -0.1321210265159607, 0.20785640180110931, -0.02834717556834221), 'leg_R':(0.9775136113166809, -0.2108728289604187, -7.621973274751781e-09, 3.417979144160199e-08)} )
# jambe droite croisée ouverte
pose_list.append( {'root':(1.0, 0.0, 0.0, 0.0), 'hip_L':(1.0, 0.0, 0.0, 0.0), 'upperleg_L':(0.991597056388855, -0.051413580775260925, -0.09881721436977386, 0.06578522175550461), 'leg_L':(0.8825993537902832, -0.4701261818408966, -7.017109737716964e-08, -6.680762965061149e-08), 'hip_R':(1.0, 0.0, 0.0, 0.0), 'upperleg_R':(0.7513235211372375, 0.2894059121608734, 0.5897215604782104, -0.06313477456569672), 'leg_R':(0.6219395995140076, -0.7830652594566345, 7.786730549241838e-08, -3.593877551111291e-08)} )
# jambe gauche croisée fermée
pose_list.append( {'root':(1.0, 0.0, 0.0, 0.0), 'hip_L':(1.0, 0.0, 0.0, 0.0), 'upperleg_L':(0.9754602313041687, 0.07017459720373154, -0.09620121866464615, -0.1851980835199356), 'leg_L':(0.8756988644599915, -0.4828576147556305, -9.087131758178657e-08, -3.9033391630027836e-08), 'hip_R':(1.0, 0.0, 0.0, 0.0), 'upperleg_R':(0.9802179932594299, -0.10994869470596313, 0.08077140152454376, 0.1433885097503662), 'leg_R':(0.9827722311019897, -0.18482141196727753, -2.3691193451469417e-10, 3.0561672303974774e-08)} )
# jambe droite croisée fermée
pose_list.append( {'root':(1.0, 0.0, 0.0, 0.0), 'hip_L':(1.0, 0.0, 0.0, 0.0), 'upperleg_L':(0.9830724596977234, -0.11871244758367538, 0.01673079840838909, -0.13854987919330597), 'leg_L':(0.9777296781539917, -0.20986871421337128, -3.52438362938301e-08, -1.1787633980020473e-08), 'hip_R':(1.0, 0.0, 0.0, 0.0), 'upperleg_R':(0.97410649061203, 0.04138699173927307, 0.1591317057609558, 0.15517999231815338), 'leg_R':(0.9522908926010132, -0.30519193410873413, 1.0268800920698595e-08, 3.3236080554388536e-08)} )

####### development #######
# print_pose( "butt_armature" )
# reset to default
load_pose( "butt_armature", pose_list[ 0 ] )
cid0 = int( mathutils.noise.random() * ( len( pose_list ) - 1 ) )
cid1 = int( mathutils.noise.random() * ( len( pose_list ) - 1 ) )
alpha = mathutils.noise.random()
print( "mixing pose", cid0, "with", cid1, ", alpha:", alpha )
# load_slerp_poses( "butt_armature", pose_list[ cid0 ], pose_list[ cid1 ], alpha )
# load_pose( "butt_armature", pose_list[ int( mathutils.noise.random() * ( len( pose_list ) - 1 ) ) ] )