import bpy
import math
import mathutils

main_conf = {}
main_conf['cube_name'] = 'cube'
main_conf['minimize_json'] = False

def collect_shapekey_data( o ):
	
	shapekey_data['obj'] = o
	shapekey_data['mesh'] = o.data
	
	# mesh level
	shapekey_data['vertices'] = shapekey_data['mesh'].vertices
	shapekey_data['faces'] = shapekey_data['mesh'].polygons
	shapekey_data['uvlayer'] = shapekey_data['mesh'].uv_layers[0]
	shapekey_data['loops'] = shapekey_data['mesh'].loops
	
	# generation of matrices
	shapekey_data['correction_pos'] = shapekey_data['obj'].matrix_world * correction_pos
	shapekey_data['axis_mat'][0] = [1,0,0,0]
	shapekey_data['axis_mat'][1] = [0,0,1,0]
	shapekey_data['axis_mat'][2] = [0,-1,0,0]
	shapekey_data['correction_pos'] = shapekey_data['axis_mat'] * shapekey_data['correction_pos']
	shapekey_data['correction_normal'] = axis_mat.copy()
	
	# generation of shapekey file path
	current_folder = bpy.context.blend_data.filepath[0:bpy.context.blend_data.filepath.rfind('/')]
	current_folder_name = current_folder[current_folder.rfind('/')+1:]
	shapekey_data['json_path'] = current_folder + '/shapekey_' + current_folder_name + '.json'
	
	# is file already generated?
	shapekey_data['json_generate'] = os.path.isfile( shapekey_data['json_path'] )

def flatten_object():
	
	mod_name = ''
	mod_to_remove = []
	for m in shapekey_data['obj'].modifiers:
		if m.type != 'SOFT_BODY':
			mod_to_remove.append( m )
		else:
			mod_name = m.name
	for m in mod_to_remove:
		shapekey_data['obj'].modifiers.remove( m )

	bpy.ops.object.select_all( action = 'DESELECT' )
	bpy.ops.object.select_pattern( pattern = shapekey_data['obj'].name )
	bpy.ops.object.modifier_apply( apply_as = 'DATA', modifier = mod_name )

shapekey_data = {
	# pointers
	'obj' : None,
	'mesh' : None, 
	# file related
	'json_generate' : False,
	'json_path' : None,
	# general
	'all_indices' : '',
	'all_pos' : '',
	'all_norms' : '',
	'all_colors' : '',
	'all_uvs' : '',
	'correction_pos' : mathutils.Matrix.Identity(4),
	'correction_normal' : mathutils.Matrix.Identity(4),
	'axis_mat' : mathutils.Matrix.Identity(4),
	# mesh specific, populate with collect_shapekey_data
	'vertices' : None,
	'faces' : None,
	'uvlayer' : None,
	'loops' : None,
	'export_faces': [],
	'edges' : [],
	'vcount' : 0,
	'ncount' : 0,
	'ccount' : 0,
	'uvcount' : 0
}

obj = bpy.data.objects[ main_conf['cube_name'] ]

collect_shapekey_data( obj )

log = ""

'''
collecting data
'''

class ExportVertice():

	def __init__( self, vid, face, force_sharp = False ):

		global edges

		self.id = vid
		self.default_face = face
		self.normal = mathutils.Vector((0,0,0))
		self.connected_edges = []
		self.connected_faces = []
		self.sharp = force_sharp

		if force_sharp:
			return

		i = 0
		l = len( edges )
		while i < l:
			e = edges[i]
			i += 1
			if e.vertices[0] == self.id or e.vertices[1] == self.id:
				self.connected_edges.append( e )
				if e.use_edge_sharp:
					self.sharp = True

	def connect_to_faces( self, efaces ):

		if self.sharp:
			return

		for e in self.connected_edges:
			for f in efaces:
				if f.is_connected_to_edge( e ):
					self.connected_faces.append( f )

	def compute_normal( self ):

		if self.sharp or len(self.connected_faces) <= 1:
			self.normal = self.default_face.normal
			return

		self.normal = mathutils.Vector((0,0,0))
		for f in self.connected_faces:
			self.normal += f.normal

		self.normal.normalize()

class ExportEdge():

	def __init__( self, vid0, vid1 ):

		self.vertices = []
		self.vertices.append( vid0 )
		self.vertices.append( vid1 )
		self.use_edge_sharp = False

class ExportFace( ExportVertice, dict ) :

	def __init__(self):

		self.verts = []
		self.edges = []
		self.normal = 0

	def is_connected_to_edge( self, e ):

		e0  = (e == self.edges[0] )
		e1  = (e == self.edges[1] )
		e2  = (e == self.edges[2] )
		return e0 or e1 or e2

	def share_edge( self, other ):

		if other is self:
			return True

		for e in self.edges:
			for oe in other.edges:
				if oe is e and not e.use_edge_sharp:
					return True

		return False

def to_string_vec3( v ):

	return str(v.x) + "," + str(v.y) + "," + str(v.z)

def to_string_co( v ):

	if len( v ) == 2:
		return str(v[0]) + "," + str(v[1])

	elif len( v ) == 3:
		return str(v[0]) + "," + str(v[1]) + "," + str(v[2])

def apply_mat_pos( co ):

	mat = mathutils.Matrix.Translation((co[0],co[1],co[2]))
	mat = shapekey_data['correction_pos'] * mat
	return mat.to_translation()

def apply_mat_normal( co ):

	mat = mathutils.Matrix.Translation((co[0],co[1],co[2]))
	mat = shapekey_data['correction_normal'] * mat
	return to_string_vec3(mat.to_translation())

def apply_mat( co ):

	return to_string_vec3(apply_mat_pos(co))

def add_vertex( vid, pos = True, norm = True, col = False ):

	vert = shapekey_data['vertices'][vid]

	if pos:
		add_pos( apply_mat( vert.co ) )
	if norm:
		add_pos( apply_mat( vert.normal ) )
	if col:
		add_color( apply_mat( vert.normal ) )

def add_pos( v ):

	if len(shapekey_data['all_pos']) != 0:
		shapekey_data['all_pos'] += ","
	shapekey_data['all_pos'] += to_string_vec3( v )
	shapekey_data['vcount'] += 1

def add_normal( v ):

	if len(shapekey_data['all_norms']) != 0:
		shapekey_data['all_norms'] += ","
	shapekey_data['all_norms'] += to_string_vec3( v )
	shapekey_data['ncount'] += 1

def add_color( v ):

	if len(shapekey_data['all_colors']) != 0:
		shapekey_data['all_colors'] += ","
	shapekey_data['all_colors'] += to_string_vec3( v )
	shapekey_data['ccount'] += 1

def add_uv( vid ):

	if len(shapekey_data['all_uvs']) != 0:
		shapekey_data['all_uvs'] += ", "
	shapekey_data['all_uvs'] += to_string_co( shapekey_data['uvlayer'].data[vid].uv )
	shapekey_data['uvcount'] += 1

def add_custom_vertex( v ):

	if len(shapekey_data['all_pos']) != 0:
		shapekey_data['all_pos'] += ","
	shapekey_data['all_pos'] += to_string_co( v )
	shapekey_data['vcount'] += 1

def compute_normal( v0, v1, v2 ):

	side_a = v0 - v1
	side_a.normalize()
	side_b = v0 - v2
	side_b.normalize()
	norm = side_a.cross( side_b )
	norm.normalize()
	return norm

def add_computed_normal( n ):

	if len(shapekey_data['all_norms']) != 0:
		shapekey_data['all_norms'] += ","
	shapekey_data['all_norms'] += to_string_co( n )
	shapekey_data['ncount'] += 1

def new_shapekey(name):

	return { 'name':name, 'vcount':0, 'vncount':0, 'vertices':'', 'vertex_normals':'' }

def check_edges( face ):

	global edges
	global faces
	global loops

	vnum = len(face.vertices)

	for i in range(0,vnum):
		e = shapekey_data['edges'][ loops[ face.loop_start+i ].edge_index ]
		if e.use_edge_sharp:
			print( "face touch a sharp edge @", shapekey_data['loops'][ face.loop_start+i ].edge_index )

def populate_edges():

	for e in shapekey_data['mesh'].edges:
		shapekey_data['edges'].append( e )

def retrieve_edges( face, vid0, vid1 ):

	global edges
	global faces
	global loops

	if len(shapekey_data['edges']) == 0:

		populate_edges()

	vnum = len(face.vertices)

	for i in range(0,vnum):
		e = shapekey_data['edges'][ shapekey_data['loops'][ face.loop_start+i ].edge_index ]
		vs = e.vertices
		if vs[0] == vid0 and vs[1] == vid1 or vs[1] == vid0 and vs[0] == vid1:
			return e

	# due to triangulation, we have to generate new edges
	e = ExportEdge( vid0, vid1 )
	shapekey_data['edges'].append( e )
	return e

def generate_faces( face, vid0, vid1, vid2 ):

	global export_faces
	global vertices
	global edges
	global faces
	global loops

	f = ExportFace()

	v0 = apply_mat_pos( vertices[vid0].co )
	v1 = apply_mat_pos( vertices[vid1].co )
	v2 = apply_mat_pos( vertices[vid2].co )

	f.normal = compute_normal( v0, v1, v2 )

	f.verts.append( ExportVertice( vid0, f, False ) )
	f.verts.append( ExportVertice( vid1, f, False ) )
	f.verts.append( ExportVertice( vid2, f, False ) )

	f.edges.append( retrieve_edges( face, vid0, vid1 ) )
	f.edges.append( retrieve_edges( face, vid1, vid2 ) )
	f.edges.append( retrieve_edges( face, vid2, vid0 ) )

	# pushing pos and uvs to final output directly
	add_pos( v0 )
	add_pos( v1 )
	add_pos( v2 )

	export_faces.append( f )

def face_based_export():
	
	global faces
	global vertices
	global log
	global all_indices
	global fcount
	global uvcount
	global export_shapekey
	global export_faces

	for face in faces:
		vs = face.vertices
		vnum = len(vs)
		if vnum == 4:
			generate_faces( face, vs[0],vs[1],vs[2] )
			generate_faces( face, vs[2],vs[3],vs[0] )
			add_uv( face.loop_start+0 )
			add_uv( face.loop_start+1 )
			add_uv( face.loop_start+2 )
			add_uv( face.loop_start+2 )
			add_uv( face.loop_start+3 )
			add_uv( face.loop_start+0 )
		if vnum == 3:
			generate_faces( face, vs[0],vs[1],vs[2] )
			add_uv( face.loop_start+0 )
			add_uv( face.loop_start+1 )
			add_uv( face.loop_start+2 )

	print( len(export_faces) )

	# connect vertices to faces
	for face in export_faces:
		for v in face.verts:
			v.connect_to_faces( export_faces )
			v.compute_normal()

	fcount = 0
	for f in export_faces:
		if len( all_indices ) != 0:
			all_indices += ","
		i = 0
		l = len( f.verts )
		while i < l:
			# add_normal(f.verts[i].normal)
			all_indices += str( fcount * 3 + i )
			if i < len( f.verts ) - 1:
				all_indices += ","
			i += 1
		fcount += 1
			
	log += "vertices added: " + str(vcount) + "\n"
	log += "faces added: " + str(fcount) + "\n"
	log += "uv added: " + str(uvcount) + "\n"

	'''
	for kb in shape_keys:

		export_shapekey = new_shapekey(kb.name)
		efid = 0

		for face in faces:

			vnum = len(face.vertices)
			vs = []

			for i in range(0,vnum):
				vs.append( apply_mat_pos( kb.data[face.vertices[i]].co ) )

			if len( export_shapekey["vertices"] ) > 0:
				export_shapekey["vertices"] += ","

			if vnum == 4:

				norm = compute_normal( vs[0],vs[1],vs[2] )
				export_faces[ efid ].normal = norm
				efid += 1

				norm = compute_normal( vs[2],vs[3],vs[0] )
				export_faces[ efid ].normal = norm
				efid += 1
				export_shapekey["vertices"] += to_string_co( vs[0] ) + ","
				export_shapekey["vertices"] += to_string_co( vs[1] ) + ","
				export_shapekey["vertices"] += to_string_co( vs[2] ) + ","
				export_shapekey["vertices"] += to_string_co( vs[2] ) + ","
				export_shapekey["vertices"] += to_string_co( vs[3] ) + ","
				export_shapekey["vertices"] += to_string_co( vs[0] )
				export_shapekey["vcount"] += 6
				export_shapekey["vncount"] += 6

			elif vnum == 3:

				norm = compute_normal( vs[0],vs[1],vs[2] )
				export_faces[ efid ].normal = norm
				efid += 1
				export_shapekey["vertices"] += to_string_co( vs[0] ) + ","
				export_shapekey["vertices"] += to_string_co( vs[1] ) + ","
				export_shapekey["vertices"] += to_string_co( vs[2] )
				export_shapekey["vcount"] += 3
				export_shapekey["vncount"] += 3
		
		for f in export_faces:

			if len( export_shapekey["vertex_normals"] ) != 0:
				export_shapekey["vertex_normals"] += ","

			i = 0
			l = len( f.verts )
			for v in f.verts:

				v.compute_normal()
				export_shapekey["vertex_normals"] += to_string_co( v.normal )
				if i < l -1:
					export_shapekey["vertex_normals"] += ","
				i += 1

		log += "shapekey " + kb.name + " added: " + str(export_shapekey["vcount"]) + " pos\n"
		all_shapekeys.append( export_shapekey )
	'''

face_based_export()

ctab = '\t'
cline = '\n'
if main_conf['minimize_json']:
	ctab = ''
	cline = ''

json_content = "{"+cline
json_content += ctab+"\"info\":{"+cline
json_content += ctab+ctab+"\"vertices\":" + str(vcount) + ","+cline
json_content += ctab+ctab+"\"normals\":" + str(ncount) + ","+cline
json_content += ctab+ctab+"\"colors\":" + str(ccount) + ","+cline
json_content += ctab+ctab+"\"faces\":" + str(fcount) + ","+cline
json_content += ctab+ctab+"\"uvs\":" + str(uvcount) + ","+cline
json_content += ctab+ctab+"\"shapekeys\":{"+cline
i = 0
for ksn in all_shapekeys:
	json_content += ctab+ctab+ctab+"\""+ksn["name"]+"\":{"+cline
	json_content += ctab+ctab+ctab+ctab+"\"vertices\":" + str(ksn["vcount"])+","+cline
	json_content += ctab+ctab+ctab+ctab+"\"vertex_normals\":" + str(ksn["vncount"])+cline
	json_content += ctab+ctab+ctab+"}"
	if i != len(all_shapekeys)-1:
		json_content += ","
	json_content += ""+cline
	i += 1
json_content += ctab+ctab+"}"+cline
json_content += ctab+"},"+cline
json_content += ctab+"\"vertices\":[" + all_pos + "],"+cline
json_content += ctab+"\"normals\":[" + all_norms + "],"+cline
json_content += ctab+"\"colors\":[" + all_colors + "],"+cline
json_content += ctab+"\"uvs\":[" + all_uvs + "],"+cline
json_content += ctab+"\"indices\":[" + all_indices + "],"+cline
json_content += ctab+"\"morphs\":{"+cline
i = 0
for ksn in all_shapekeys:
	json_content += ctab+ctab+"\""+ ksn["name"] +"\":{"+cline
	json_content += ctab+ctab+ctab+"\"vertices\":[" + ksn["vertices"] +"],"+cline
	json_content += ctab+ctab+ctab+"\"vertex_normals\":[" + ksn["vertex_normals"] +"]"+cline
	json_content += ctab+ctab+"}"
	if i != len(all_shapekeys)-1:
		json_content += ","
	json_content += ""+cline
	i += 1
		
json_content += ctab+"}"+cline
json_content += "}"+cline

f = open( json_path, "w+" )
f.write( json_content )
f.close()

#print( json_content )
#print( log )
print( "************** generation of shapekey " + json_path + " done **************" )