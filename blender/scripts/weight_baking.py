import bpy
import colorsys
import os

wname = 'cube_diffuse'
wwidth = 1024
wheight = 1024

do_bake = True

bpy.data.objects['floor'].location[2] = -100

if not wname in bpy.data.images:
	bpy.ops.image.new(name=wname, width=wwidth, height=wheight, color=(0.0, 0.0, 0.0, 1.0), alpha=False, generated_type='BLANK', float=True)

wmap = bpy.data.images[wname]
wmap.filepath = '//' + os.path.basename(bpy.data.filepath)[:-6] + '_diffuse.png'

print( (wwidth*wheight), len( wmap.pixels ), len( wmap.pixels ) / (wwidth*wheight) )

'''
newpixels = [0.0] * len( wmap.pixels )
pcount = len( wmap.pixels )
for y in range( 0, wheight ):
	for x in range( 0, wwidth ):
		pixid = x + y * wwidth
		rid = ( pixid * 4 )
		gid = ( pixid * 4 ) + 1
		bid = ( pixid * 4 ) + 2
		aid = ( pixid * 4 ) + 3
		newpixels[ rid ] = x / wwidth
		newpixels[ gid ] = y / wheight
		newpixels[ aid ] = 1.0
wmap.pixels = newpixels
'''

if do_bake:

	cube = bpy.data.objects['cube']
	material = cube.material_slots[0].material

	#bpy.ops.object.select_pattern(pattern="cube")
	#bpy.context.scene.objects.active = cube
	#bpy.context.active_object = cube
	#cube.active_material_index = 0
	#bpy.ops.node.add_node(type="ShaderNodeTexImage", use_transform=True)

	nodes = material.node_tree.nodes
	node = nodes.new('ShaderNodeTexImage')
	node.name = 'bake_target'
	node.image = wmap

	for i in range(0, 20):
		if i == 1:
			bpy.context.scene.layers[i] = True
		else:
			bpy.context.scene.layers[i] = False

	bpy.context.scene.frame_current = 0
	cube.select = True
	bpy.context.scene.objects.active = cube
	bpy.ops.object.bake(type='DIFFUSE')

wmap.save()

quit()