import os

hotfolder = '../generated'
folder_suffix_flag = '_done'
needle = 'render-file.py'

search_string = 'frame_step = 1'
replace_string = 'frame_step = 200'

folders = sorted([os.path.join(hotfolder, o) for o in os.listdir(hotfolder) 
	if os.path.isdir(os.path.join(hotfolder,o))])

found = 0
for f in folders:
	if f[-len(folder_suffix_flag):] != folder_suffix_flag:
		files = os.listdir( f )
		for fi in files:
			if fi == needle:
				found += 1
				filedata = ''
				with open( os.path.join( hotfolder, f, fi ), 'r') as pyf :
					filedata = pyf.read()
					filedata = filedata.replace( search_string, replace_string )
					pyf.close()
				with open( os.path.join( hotfolder, f, fi ), 'w') as pyf:
					pyf.write(filedata)
					pyf.close()