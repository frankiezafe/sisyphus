import bpy
import pickle

obj_name = 'cube'
mod_name = 'Softbody'

mod = bpy.data.objects[ obj_name ].modifiers[ mod_name ]

#submod = mod.settings
#base = 'bpy.data.objects[\\\'\' + obj_name + \'\\\'].modifiers[\\\'\' + mod_name + \'\\\']'
#subbase = 'settings'

submod = mod.point_cache
base = 'bpy.data.objects[\\\'\' + obj_name + \'\\\'].modifiers[\\\'\' + mod_name + \'\\\']'
subbase = 'point_cache'

step = 2

keys = dir(submod)
skip_keys = [ '__', 'rotation_estimate', 'scale_estimate', 'location_mass_center', 'bl_rna', 'rna_type', 'effector_weights', 'info', 'point_caches', 'is_' ]
string_keys = [ 'vertex_group_goal', 'collision_type', 'aerodynamics_type', 'vertex_group_spring', 'vertex_group_mass', 'compression', 'filepath', 'name' ]

if step == 1:
	# generation of second part
	print( '******* python generation #1 *******' )
	for k in keys:
		keepon = True
		for skk in skip_keys:
			if k[:len(skk)] == skk:
				keepon = False
				break
		if not keepon:
			continue
		is_string = False
		for stk in string_keys:
			if k == stk:
				is_string = True
				break
		out = 'out += \'' + base + '.' + subbase + '.' + k + ' = '
		if is_string:
			out += '\\\''
		out += '\'+ str( mod.' + subbase + '.' + k + ' ) + \''
		if is_string:
			out += '\\\''
		out += '\\n\''
		print( out )

elif step == 2:
	print( '******* python generation #2 *******' )
	out = ''
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.aero = '+ str( mod.settings.aero ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.aerodynamics_type = \''+ str( mod.settings.aerodynamics_type ) + '\'\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.ball_damp = '+ str( mod.settings.ball_damp ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.ball_size = '+ str( mod.settings.ball_size ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.ball_stiff = '+ str( mod.settings.ball_stiff ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.bend = '+ str( mod.settings.bend ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.choke = '+ str( mod.settings.choke ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.collision_group = '+ str( mod.settings.collision_group ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.collision_type = \''+ str( mod.settings.collision_type ) + '\'\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.damping = '+ str( mod.settings.damping ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.error_threshold = '+ str( mod.settings.error_threshold ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.friction = '+ str( mod.settings.friction ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.fuzzy = '+ str( mod.settings.fuzzy ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.goal_default = '+ str( mod.settings.goal_default ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.goal_friction = '+ str( mod.settings.goal_friction ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.goal_max = '+ str( mod.settings.goal_max ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.goal_min = '+ str( mod.settings.goal_min ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.goal_spring = '+ str( mod.settings.goal_spring ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.gravity = '+ str( mod.settings.gravity ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.mass = '+ str( mod.settings.mass ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.plastic = '+ str( mod.settings.plastic ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.pull = '+ str( mod.settings.pull ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.push = '+ str( mod.settings.push ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.shear = '+ str( mod.settings.shear ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.speed = '+ str( mod.settings.speed ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.spring_length = '+ str( mod.settings.spring_length ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.step_max = '+ str( mod.settings.step_max ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.step_min = '+ str( mod.settings.step_min ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_auto_step = '+ str( mod.settings.use_auto_step ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_diagnose = '+ str( mod.settings.use_diagnose ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_edge_collision = '+ str( mod.settings.use_edge_collision ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_edges = '+ str( mod.settings.use_edges ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_estimate_matrix = '+ str( mod.settings.use_estimate_matrix ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_face_collision = '+ str( mod.settings.use_face_collision ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_goal = '+ str( mod.settings.use_goal ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_self_collision = '+ str( mod.settings.use_self_collision ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.use_stiff_quads = '+ str( mod.settings.use_stiff_quads ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.vertex_group_goal = \''+ str( mod.settings.vertex_group_goal ) + '\'\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.vertex_group_mass = \''+ str( mod.settings.vertex_group_mass ) + '\'\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].settings.vertex_group_spring = \''+ str( mod.settings.vertex_group_spring ) + '\'\n'
	
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.compression = \''+ str( mod.point_cache.compression ) + '\'\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.filepath = \''+ str( mod.point_cache.filepath ) + '\'\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.frame_end = '+ str( mod.point_cache.frame_end ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.frame_start = '+ str( mod.point_cache.frame_start ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.frame_step = '+ str( mod.point_cache.frame_step ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.index = '+ str( mod.point_cache.index ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.name = \''+ str( mod.point_cache.name ) + '\'\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.use_disk_cache = '+ str( mod.point_cache.use_disk_cache ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.use_external = '+ str( mod.point_cache.use_external ) + '\n'
	out += 'bpy.data.objects[\'' + obj_name + '\'].modifiers[\'' + mod_name + '\'].point_cache.use_library_path = '+ str( mod.point_cache.use_library_path ) + '\n'

	print( out )

'''
bpy.data.objects['cube'].modifiers['Softbody'].settings.aero = 0
bpy.data.objects['cube'].modifiers['Softbody'].settings.aerodynamics_type = 'SIMPLE'
bpy.data.objects['cube'].modifiers['Softbody'].settings.ball_damp = 0.0010000000474974513
bpy.data.objects['cube'].modifiers['Softbody'].settings.ball_size = 0.6000000238418579
bpy.data.objects['cube'].modifiers['Softbody'].settings.ball_stiff = 1.0
bpy.data.objects['cube'].modifiers['Softbody'].settings.bend = 3.3999996185302734
bpy.data.objects['cube'].modifiers['Softbody'].settings.choke = 3
bpy.data.objects['cube'].modifiers['Softbody'].settings.collision_group = None
bpy.data.objects['cube'].modifiers['Softbody'].settings.collision_type = 'AVERAGE'
bpy.data.objects['cube'].modifiers['Softbody'].settings.damping = 11.599998474121094
bpy.data.objects['cube'].modifiers['Softbody'].settings.error_threshold = 0.10000000149011612
bpy.data.objects['cube'].modifiers['Softbody'].settings.friction = 4.0
bpy.data.objects['cube'].modifiers['Softbody'].settings.fuzzy = 0
bpy.data.objects['cube'].modifiers['Softbody'].settings.goal_default = 0.3999999761581421
bpy.data.objects['cube'].modifiers['Softbody'].settings.goal_friction = 5.899998664855957
bpy.data.objects['cube'].modifiers['Softbody'].settings.goal_max = 0.5999999642372131
bpy.data.objects['cube'].modifiers['Softbody'].settings.goal_min = 0.0
bpy.data.objects['cube'].modifiers['Softbody'].settings.goal_spring = 0.19999998807907104
bpy.data.objects['cube'].modifiers['Softbody'].settings.gravity = 9.800000190734863
bpy.data.objects['cube'].modifiers['Softbody'].settings.mass = 0.10000000149011612
bpy.data.objects['cube'].modifiers['Softbody'].settings.plastic = 5
bpy.data.objects['cube'].modifiers['Softbody'].settings.pull = 0.4000000059604645
bpy.data.objects['cube'].modifiers['Softbody'].settings.push = 0.7999999523162842
bpy.data.objects['cube'].modifiers['Softbody'].settings.shear = 0.20000000298023224
bpy.data.objects['cube'].modifiers['Softbody'].settings.speed = 1.0
bpy.data.objects['cube'].modifiers['Softbody'].settings.spring_length = 0
bpy.data.objects['cube'].modifiers['Softbody'].settings.step_max = 300
bpy.data.objects['cube'].modifiers['Softbody'].settings.step_min = 10
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_auto_step = True
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_diagnose = False
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_edge_collision = False
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_edges = True
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_estimate_matrix = False
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_face_collision = False
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_goal = True
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_self_collision = True
bpy.data.objects['cube'].modifiers['Softbody'].settings.use_stiff_quads = True
bpy.data.objects['cube'].modifiers['Softbody'].settings.vertex_group_goal = 'rigids'
bpy.data.objects['cube'].modifiers['Softbody'].settings.vertex_group_mass = ''
bpy.data.objects['cube'].modifiers['Softbody'].settings.vertex_group_spring = 'rigids'
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.compression = 'NO'
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.filepath = ''
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.frame_end = 100
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.frame_start = 1
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.frame_step = 1
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.index = -1
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.is_baked = False
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.is_baking = False
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.is_frame_skip = False
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.is_outdated = True
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.name = ''
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.use_disk_cache = False
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.use_external = False
bpy.data.objects['cube'].modifiers['Softbody'].point_cache.use_library_path = True

bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.aero = 0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.aerodynamics_type = 'SIMPLE'
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.ball_damp = 0.5
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.ball_size = 0.49000000953674316
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.ball_stiff = 1.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.bend = 10.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.choke = 3
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.collision_group = None
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.collision_type = 'AVERAGE'
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.damping = 5.3000006675720215
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.error_threshold = 0.10000000149011612
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.friction = 4.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.fuzzy = 0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.goal_default = 0.699999988079071
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.goal_friction = 0.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.goal_max = 1.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.goal_min = 0.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.goal_spring = 0.5
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.gravity = 9.800000190734863
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.mass = 9.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.plastic = 0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.pull = 0.5
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.push = 0.5
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.shear = 1.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.speed = 1.0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.spring_length = 0
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.step_max = 300
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.step_min = 10
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_auto_step = True
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_diagnose = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_edge_collision = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_edges = True
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_estimate_matrix = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_face_collision = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_goal = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_self_collision = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.use_stiff_quads = True
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.vertex_group_goal = ''
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.vertex_group_mass = 'weights'
bpy.data.objects['butt_physic'].modifiers['Softbody'].settings.vertex_group_spring = ''
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.compression = 'NO'
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.filepath = ''
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.frame_end = 100
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.frame_start = 1
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.frame_step = 1
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.index = -1
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.is_baked = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.is_baking = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.is_frame_skip = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.is_outdated = True
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.name = ''
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.use_disk_cache = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.use_external = False
bpy.data.objects['butt_physic'].modifiers['Softbody'].point_cache.use_library_path = True
'''
