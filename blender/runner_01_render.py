import os, re, subprocess, colorsys
from time import gmtime, strftime
from PIL import Image,ImageFilter

blend_exec = '/opt/blender-2.79b-linux-glibc219-x86_64/./blender'
weight_baker = os.path.abspath( 'scripts/weight_baking.py' )
hotfolder = os.path.abspath( 'generated' )

folder_suffix_flag = '_done'
needle = 'render-file.py'
diffuse_needle = '_diffuse.png'

def boost_contrast( input_path, output_path = None ):
	im = Image.open( input_path )
	ld = im.load()
	width, height = im.size
	for y in range(height):
		for x in range(width):
			r,g,b = ld[x,y]
			if r + g + b == 0:
				continue
			higher = 0
			value = r
			if g > value:
				higher = 1
				value = g
			if b > value:
				higher = 2
				value = b
			mult = 255.0 / ld[x,y][higher]
			ld[x,y] = ( int(r*mult), int(g*mult), int(b*mult) )
			'''
			h,s,v = colorsys.rgb_to_hsv(r/255., g/255., b/255.)
			h = (h + -90.0/360.0) % 1.0
			s = s**0.65
			r,g,b = colorsys.hsv_to_rgb(h, s, 1)
			ld[x,y] = (int( v * 255 ),int( v * 255 ),int( v * 255 ))
			ld[x,y] = (int( s * 255 ),int( s * 255 ),int( s * 255 ))
			ld[x,y] = (int(r * 255.9999), int(g * 255.9999), int(b * 255.9999))
			'''
	im = im.filter( ImageFilter.BLUR )
	out = output_path
	if out == None:
		out = input_path + '.png'
	im.save( out,"PNG" )

def launch_rendering( path ):
	files = os.listdir( path )
	oktogo = False
	diffuse_needle_found = False
	blend_file = None
	for f in files:
		if f == needle:
			oktogo = True
		elif f[-len(diffuse_needle):] == diffuse_needle:
			diffuse_needle_found = True
		elif f[-6:] == '.blend':
			blend_file = f
	
	if oktogo == True:
		
		print( "############# starting model generation " + path + "#############" )
		p1 = subprocess.Popen( [ 'python', os.path.join( path, needle ) ] )
		p1.wait()
		
		print( "############# starting diffuse generation " + path + "#############" )
		if not diffuse_needle_found and blend_file is not None:
			print( "processing " + path + ' > ' + blend_file )
			print( os.path.join( path, blend_file ), weight_baker )
			p1 = subprocess.Popen( [ blend_exec, os.path.join( path, blend_file ), '-P', weight_baker ] )
			p1.wait()
		# seeking diffuse
		diffuse = None
		files = [f for f in os.listdir(path)]
		for f in files:
			if f[-len(diffuse_needle):] == diffuse_needle:
				diffuse = f
				break
		if diffuse is not None:
			absp = os.path.join( path, diffuse )
			boost_contrast( absp, absp ) # overwrite
		
		os.rename( path, path + folder_suffix_flag )


folders = sorted([os.path.join(hotfolder, o) for o in os.listdir(hotfolder) 
	if os.path.isdir(os.path.join(hotfolder,o))])

for f in folders:
	# double check
	if os.path.isdir(f) is True:
		if f[len(f)-len(folder_suffix_flag):] != folder_suffix_flag:
			launch_rendering( f )