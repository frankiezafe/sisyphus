import os, re, subprocess, colorsys
from time import gmtime, strftime
from PIL import Image,ImageFilter

overwrite = True

hotfolder = '/home/frankiezafe/projects/sisyphus/private/models/generator/generated'
needle = '_legs.json'

blend_exec = '/opt/blender-2.79-linux-glibc219-x86_64/./blender'
script_path = '/home/frankiezafe/projects/sisyphus/private/models/generator/avatar_leg_animations_exporter.py'

folders = sorted([os.path.join(hotfolder, o) for o in os.listdir(hotfolder) 
	if os.path.isdir(os.path.join(hotfolder,o))])

for d in folders:
	if os.path.isdir(d) is True:
		files = [f for f in os.listdir(d)]
		needle_found = False
		blend_file = None
		for f in files:
			#print( f[-len(needle):] )
			if f[-len(needle):] == needle:
				needle_found = not overwrite
			elif f[-6:] == '.blend':
				blend_file = f
		if not needle_found and blend_file is not None:
			print( "processing " + d + ' > ' + blend_file )
			p1 = subprocess.Popen( [ blend_exec, os.path.join(d,blend_file), '-P', script_path ] )
			p1.wait()
		# seeking diffuse
		print( "folder " + d + " finished" )