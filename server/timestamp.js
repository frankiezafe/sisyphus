var fs = require('fs');
var moment = require('moment');
var http = require('http');

// production configuration
var max_days = 0;
var max_hours = 3;
var max_minutes = 0;
var max_seconds = 0;
var max_millis = 0;

// local configuration
// var max_days = 0;
// var max_hours = 0;
// var max_minutes = 0;
// var max_seconds = 30;
// var max_millis = 0;

var revist_after = 5000;

// duration in milliseconds
var maximum_duration =
  (
    max_days * 86400 +
    max_hours * 3600 +
    max_minutes * 60 +
    max_seconds
  ) * 1000 + max_millis;

var persistent_folder = 'sisyphus.persistent/';
var inventory_folder = 'sisyphus.inventory/';

var timestamp_path = persistent_folder + 'lasttime';
var shape_path = persistent_folder + 'shape';
var list_of_shapes = [
  "crash1","crash2","crash3",
  "crash4","crash5","crash6",
  "crash7"
];
// anti log duplication
var last_created_file = "";

function extract_time(callback) {

  var _struct = {
    'now': undefined,
    'now_str': undefined,
    'lasttime':undefined,
    'lasttime_str': undefined,
    'message' : "",
    'delta' : undefined,
    'delta_millis' : 0,
    'previous_shape' : "",
    'current_shape' : "",
    'shape_swap' : false
  };

  _struct['now'] = new moment();
  _struct['now_str'] = _struct['now'].format(moment.HTML5_FMT.DATETIME_LOCAL_MS);

  // console.log('extract_time >', callback );
  fs.readFile( timestamp_path, function(err, data) {
    if (err) {
      _struct['message'] = "creation of a timestmap";
      write_time(callback, _struct);
      return;
    }
    _struct['lasttime_str'] = data.toString('utf8');
    _struct['lasttime'] = new moment( _struct['lasttime_str'] );
    _struct['delta'] = moment.duration( _struct['now'].diff( _struct['lasttime'] ) );
    _struct['delta_millis'] = _struct['delta'].asMilliseconds();
    if ( _struct['delta_millis'] > maximum_duration ) {
      _struct['message'] = "too long, reseting the timestamp";
      // console.log('TOO LATE!!!, creation of a new log' );
      write_time(callback, _struct);
    } else {
      _struct['message'] = "below the maximum time";
      // console.log('ok, still not too late' );
      extract_shape(callback, _struct);
    }

  });

}

function write_time(callback, _struct) {
  // console.log('write_time >', callback );
  fs.writeFile( timestamp_path, _struct['now_str'], function (err) {
    extract_shape(callback, _struct);
  });
}

function extract_shape(callback, _struct) {

  fs.readFile( shape_path, function(err, data) {
    if (err) {
      _struct['current_shape'] = list_of_shapes[0];
      _struct['message'] += ", selection of shape " + _struct['current_shape'];
      write_shape(callback, _struct);
      return;
    }
    _struct['current_shape'] = data.toString('utf8');
    if ( _struct['delta_millis'] > maximum_duration ) {
      for( var i = 0; i < list_of_shapes.length; ++i ) {
        if ( i == list_of_shapes.length - 1 ) {
          _struct['previous_shape'] = list_of_shapes[list_of_shapes.length - 1];
          _struct['current_shape'] = list_of_shapes[0];
          break;
        }
        if ( _struct['current_shape'] == list_of_shapes[i] ) {
          _struct['previous_shape'] = list_of_shapes[i];
          _struct['current_shape'] = list_of_shapes[i+1];
          break;
        }
      }
      _struct['message'] += ", switching to shape " + _struct['current_shape'];
      store_shape( callback, _struct );
    } else {
      _struct['message'] += ", keeping shape " + _struct['current_shape'];
      report(callback, _struct);
    }
  });
}

function write_shape(callback, _struct) {
  // console.log('write_time >', callback );
  fs.writeFile( shape_path, _struct['current_shape'], function (err) {
    report(callback, _struct);
  });
}

function store_shape(callback, _struct) {

  var path = inventory_folder + "log_" +
    _struct['lasttime'].format(moment.HTML5_FMT.DATETIME_LOCAL_MS);

  if (
    _struct['previous_shape'] == "" ||
    last_created_file == path
  ) {
    write_shape(callback, _struct);
    return;
  }

  _struct['shape_swap'] = true;
  last_created_file = path;

  var content = "";
  content += _struct['previous_shape'] + "|";
  content += _struct['lasttime'].format("DD/MM/YY") + "|";
  var secs = _struct['delta_millis'] / 1000;
  var d = Math.floor( secs / ( 3600 * 24 ) );
  secs -= d * ( 3600 * 24 );
  var h = Math.floor( secs / 3600 );
  secs -= h * 3600;
  var m = Math.floor( secs / 60 );
  secs -= m * 60;
  content += d + "-" + h + "-" + m + "-" + Math.floor( secs );
  _struct['delta_millis'] = 0;

  fs.writeFile( path, content, function (err) {
    write_shape(callback, _struct);
  });

}

function report( callback, _struct ) {

  // console.log('report >', callback );
  // console.log('datetime >', now_str );
  // console.log('lasttime >', lasttime_str );
  // console.log('delta >', delta_millis );
  // console.log('maximum duration >', maximum_duration );

  if (callback) {
    var export_struct = {
      "delta": _struct['delta_millis'],
      "max": maximum_duration,
      "speed": 1000 / maximum_duration,
      "revisit": revist_after,
      "shape": _struct['current_shape'],
      "shape_swap": _struct['shape_swap'],
      "info": _struct['message']
    };
    callback(export_struct);
  }
}

http.createServer(function(req, res) {
  extract_time( function (data) {
    res.writeHead(200, {
        // 'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Origin': 'http://sisyphus.technofle.sh',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type': 'application/json'
      });
    res.end(JSON.stringify(data, null, 3));
  } );
}).listen(3001, "127.0.0.1");

console.log('Timestamp server running at http://127.0.0.1:3001/');
