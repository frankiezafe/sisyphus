import os, shutil

source_folder = '/home/frankiezafe/projects/sisyphus/private/models/generator/generated'
folder_marker = '_done'
needles = [ '_icon.png', '_morph.json', '_diffuse.png' ]
json_root = 'models/'

sources = []
jsons = []

for root, subfolder, files in os.walk( source_folder ):
	
	for sf in subfolder:
		if sf[-5:] == folder_marker:
			p = os.path.join( root, sf )
			sources.append( p )

json = open( 'sisyphus_cubes.json', 'w' )
json.write( '{\n\t"data":[' )
model_count = 0

sources.sort()

for s in sources:
	
	files = [f for f in os.listdir(s) if os.path.isfile(os.path.join(s, f))]
	founds = []
	
	for f in files:
		for n in needles:
			if f[-len(n):] == n:
				founds.append( os.path.join( s, f ) )
				
	if len( founds ) == len( needles ):
		
		founds.sort()
		
		base = founds[0]
		for n in needles:
			if base[-len(n):] == n:
				base = base[:-len(n)]
		
		base = os.path.basename( base )
		print( base )
		
		# copying files here
		
		json_name = '"name": "' + base + '"'
		json_json = ''
		json_icon = ''
		json_diff = ''
		
		for f in founds:
			
			target = os.path.basename( f )
			shutil.copyfile( f, target )
			
			if f[-len(needles[0]):] == needles[0]:
				json_icon = '"icon": "' + json_root + target + '"'
			elif f[-len(needles[1]):] == needles[1]:
				json_json = '"morph": "' + json_root + target + '"'
			elif f[-len(needles[2]):] == needles[2]:
				json_diff = '"diffuse": "' + json_root + target + '"'
		
		if not( json_json == '' or json_icon == '' ):
			if model_count > 0:
				json.write( ',' )
			json.write( '\n\t\t{' )
			json.write( '\n\t\t\t' + json_name )
			if json_json != '':
				json.write( ',\n\t\t\t' + json_json )
			if json_diff != '':
				json.write( ',\n\t\t\t' + json_diff )
			if json_icon != '':
				json.write( ',\n\t\t\t' + json_icon )
			json.write( '\n\t\t}' )
			model_count += 1
		
json.write( '\n\t]\n}' )

'''
json = open( 'sisyphus_cubes.json', 'w' )
json.write( '{\n\t"data":[' )

fnum = 0
for dirname, dirnames, filenames in os.walk('.'):
	filenames.sort()
	for f in filenames:
		if f[-11:] == '_morph.json':
			if fnum > 0:
				json.write( ',' )
			json.write( '\n\t\t' )
			json.write( '"models/' + f + '"' )
			fnum += 1	
json.write( '\n\t]\n}' )
'''