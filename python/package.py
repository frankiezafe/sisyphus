'''
REQUIREMENT

install nodejs
$ sudo apt install nodejs
$ sudo apt-get install nodejs-legacy

install uglify-js
$ sudo npm install -g uglify-js
$ sudo npm install -g uglifyjs-webpack-plugin
'''

import os
import shutil
import errno
import re
import uuid
import subprocess
import ftplib

'''
CONFIGURATION
'''

css_files = [ 
	'css/normalize.css',
	'css/range-slider.css',
	'css/style.css'
]

threejs_files = [
	'build/three.min.js',
	'js/threejs/loaders/ColladaLoader.js',
	'js/threejs/controls/OrbitControls.js',
	'js/threejs/Detector.js',
	'js/threejs/libs/stats.min.js',
	'js/threejs/libs/dat.gui.min.js',
	'js/threejs/shaders/UnpackDepthRGBAShader.js',
	'js/threejs/utils/ShadowMapViewer.js'
]

export_folder = '../package'
export_file_css = 'sisyphus_#.css'
export_file_threejs = 'three_#.js'
export_files = [ 
	{ 
		'html_tmpl':	'template.html', 
		'js_src' : [
			'js/utils/Basics.js',
			'js/utils/Interpolation.js',
			'js/utils/MorphableMesh.js',
			'js/Ground.js',
			'js/BoneScalePreset.js',
			'js/SysiphusBone.js',
			'js/SysiphusAvatar.js',
			'js/PantonPouf.js',
			'js/TimeManager.js',
			'js/ObjectTracker.js',
			'js/main.js'
		], 
		'html_dst':		'production.html', 
		'js_dst':		'sisyphus_prod_#.js' 
	},
	{ 
		'html_tmpl':	'template_archive.html', 
		'js_src' : [
			'js/kiosk.js',
			'js/slider/range-slider.js',
			'js/utils/Basics.js',
			'js/utils/Interpolation.js',
			'js/utils/MorphableMesh.js',
			'js/Ground.js',
			'js/BoneScalePreset.js',
			'js/SysiphusBone.js',
			'js/SysiphusAvatar.js',
			'js/Cube.js',
			'js/TimeManager.js',
			'js/ObjectTracker.js',
			'js/main_avatar_only.js'
		], 
		'html_dst':		'kiosk.html', 
		'js_dst':		'sisyphus_kiosk_#.js' 
	},
	{
		'html_tmpl': 	'template_archive.html', 
		'js_src' : [
			'js/slider/range-slider.js',
			'js/utils/Basics.js',
			'js/utils/Interpolation.js',
			'js/utils/MorphableMesh.js',
			'js/Ground.js',
			'js/BoneScalePreset.js',
			'js/SysiphusBone.js',
			'js/SysiphusAvatar.js',
			'js/Cube.js',
			'js/TimeManager.js',
			'js/ObjectTracker.js',
			'js/main_avatar_only.js'
		],
		'html_dst':		'index.html', 
		'js_dst':		'sisyphus_archive_#.js'
	}
]

# pairs of strings [needle, replace] to find/replace
# in custom javascript
js_replace = [
	[
		'var skip_splash = true;',
		'var skip_splash = false;'
	],
	[
		'var remote_timestamp_server = "server/json/timestamp.json";',
		'var remote_timestamp_server = "http://51.15.234.17/timestamp";'
	],
	[
		'var remote_gallery_server = "server/json/gallery.json";',
		'var remote_gallery_server = "http://51.15.234.17/gallery";'
	],
	[
		"var remote_timestamp_server = 'server/json/timestamp.json';",
		"var remote_timestamp_server = 'http://51.15.234.17/timestamp';"
	],
	[
		"var remote_gallery_server = 'server/json/gallery.json';",
		"var remote_gallery_server = 'http://51.15.234.17/gallery';"
	]
]

#copy_dirs = [ 'fonts', 'models', 'presets', 'textures', 'gallery' ]
copy_dirs = [ 
	{ 
		'src':'fonts',
		'overwrite': False
	},
	{
		'src': 'presets',
		'overwrite': True
	},
	{
		'src': 'models',
		'overwrite': False
	},
	{
		'src': 'textures',
		'overwrite': True
	}
]
create_dir = []
copy_files = [
	['server/.htaccess','.htaccess']
]

license = ''

'''
GLOBAL VARS
'''

css_content = ''
threejs_content = ''

def copy(src, dst):
	try:
		shutil.copytree(src, dst)
	except OSError as exc: # python >2.5
		if exc.errno == errno.ENOTDIR:
			shutil.copy(src, dst)
		else:
			raise

def copyfileobj_example(source, dest, buffer_size=1024*1024):
	"""
	Copy a file from source to dest. source and dest
	must be file-like objects, i.e. any object with a read or
	write method, like for example StringIO.
	"""
	while True:
		copy_buffer = source.read(buffer_size)
		if not copy_buffer:
			break
		dest.write(copy_buffer)

def remove_comments(string):
	pattern = r"(\".*?\"|\'.*?\')|(/\*.*?\*/|//[^\r\n]*$)"
	# first group captures quoted strings (double or single)
	# second group captures comments (//single-line or /* multi-line */)
	regex = re.compile(pattern, re.MULTILINE|re.DOTALL)
	def _replacer(match):
		# if the 2nd group (capturing comments) is not None,
		# it means we have captured a non-quoted (real) comment string.
		if match.group(2) is not None:
			return "" # so we will return empty to remove the comment
		else: # otherwise, we will return the 1st group
			return match.group(1) # captured quoted-string
	return regex.sub(_replacer, string)

def get_content( filelist, type ):
	out = ""
	complete = True
	if type == 'js':
		complete = False
	for f in filelist:
		with open( f, 'rb' ) as content_file:
			out +=  minimize_string( remove_comments( content_file.read() ), complete )
	return out

def minimize_string( s, complete = True ):
	if complete:
		return s.replace('  ', '').replace('\t', '').replace('\n', '').replace('\r', '')
	else:
		return s

def uglify( path ):
	subprocess.call(["uglifyjs", path, "--compress", "--ie8", "--output", path ])

# cleaning the output path
print( "cleaning up destination folder '" + export_folder + "'" )
for root, dirs, files in os.walk( export_folder ):
	for f in files:
		os.unlink(os.path.join( root, f ))
	for d in dirs:
		rmv = True
		for cd in copy_dirs:
			if d == cd[ 'src' ] and not cd[ 'overwrite' ]:
				rmv = False
				break
		if rmv:
			shutil.rmtree(os.path.join( root, d ))
	break

# copying all dirs
for d in copy_dirs:
	newpath = os.path.join( export_folder, d['src'] )
	if d[ 'overwrite' ] or not os.path.isdir( newpath ):
		print( "copying folder '" + newpath + "' in destination folder" )
		copy( d['src'], newpath )
	else:
		print( "folder '" + newpath + "' is already present in destination folder" )

# creation of directories in package folder
for d in create_dir:
	newpath = os.path.join( export_folder, d )
	print( "creating folder '" + newpath + "'" )
	if not os.path.exists(newpath):
		os.makedirs(newpath)

# copying all files
for d in copy_files:
	infife = d[0]
	outfife = os.path.join( export_folder, d[0] )
	if len( d ) == 2:
		outfife = os.path.join( export_folder, d[1] )
	print( "copying file '" + infife + "' in '" + outfife + "'"  )
	with open(infife, 'rb') as src, open( outfife, 'wb') as dst:
		copyfileobj_example(src, dst)

# generation of common files, CSS
css_content = get_content( css_files, 'css' )
css_content = css_content.replace('../', '')
export_file_css = export_file_css.replace( "#", str(uuid.uuid1()) )
export_path = os.path.join( export_folder, export_file_css )
print( "creating css '" + export_path + "'" )
with open( export_path, 'w') as f:
	f.write( css_content )
	f.close()

# generation of common files, JS
threejs_content = get_content( threejs_files, 'js' )
export_file_threejs = export_file_threejs.replace( "#", str(uuid.uuid1()) )
export_path = os.path.join( export_folder, export_file_threejs )
print( "creating javascript '" + export_file_threejs + "'" )
with open( export_path , 'w') as f:
	f.write( threejs_content )
	f.close()
	uglify( export_path )

# generation of custom files

for ef in export_files:
	
	customjs_content = get_content( ef['js_src'], 'js' )
	for pair in js_replace:
		customjs_content = customjs_content.replace( pair[0], pair[1] )
	
	export_file_customjs = ef['js_dst'].replace( '#', str(uuid.uuid1()) )
	export_path = os.path.join( export_folder, export_file_customjs )
	print( "creating javascript '" + export_path + "'" )
	with open( export_path, 'w') as f:
		f.write( customjs_content )
		f.close()
		uglify( export_path )
	
	template_content = open( ef['html_tmpl'], 'r').read()
	template_content = template_content.replace('<!--INCLUDE_CSS-->', '<link rel="stylesheet" href="'+export_file_css+'"/>')
	template_content = template_content.replace('<!--INCLUDE_JS-->', '<script src="'+export_file_threejs+'"></script><script src="'+export_file_customjs+'"></script>')
	print( "creating html '" + export_folder+'/'+ef['html_dst'] + "'" )
	with open( os.path.join( export_folder, ef['html_dst'] ) , 'w') as f:
		f.write( template_content )
		f.close()
	
print( 'packaging successfull' )